<?php
session_start();
header("Cache-Control: no-cache, must-revalidate"); //para borrar cache
if (!isset($_SESSION['proveed'])) :
    include_once('index.php');
else : 
    $Gin = $_SESSION['gin'];
    $usuario=$_SESSION['proveed']
?>
<!doctype html>
<html lang="es en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>LIQUIDATIONS</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="main.css">
        
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>
        <!-- Scripts -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
   
        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

         <!-- librerias necesarias para finalizar sesion por inactividad -->
         <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>
        
        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>
        <script type="text/javascript" src="purchases.js?v=<?php echo time(); ?>"></script>
        <!-- Terminan Scripts -->
            
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>
              
                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="purchases_v2.php">Liquidations</a></li>
                        <li><a class="dropdown-item" href="doproveed_v2.php">Delivery Order</a></li>
                    </ul>

                    <div id="gin" style="display:none;">
                        <input id="GinID" value="<?php echo $Gin; ?>"/>
                        <input id="usuario" value="<?php echo $usuario; ?>"/>
                    </div> 
                </div>

                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-1 col-sm-2 col-xs-2" href="purchases_v2.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA </div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                   <!--  <p> LIQUIDATIONS </p> -->
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                <!-- Botones -->
                </div>

                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"  value="<?php echo $_SESSION['proveed']; ?>"><?php echo $_SESSION['proveed']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- contenido -->
        </br></br>
        <div class="d-flex justify-content-center">
            <div class="card col-9"  style="background-color: #17562c94; color: #ffffff;">
                <div class="card-header"> <!-- style="background-color: #17562c; color: #ffffff;" 17562cba -->
                    <b>LIQUIDATIONS</b>
                </div></br>
                <div class="card-body1  d-flex justify-content-center">
                    <form id="formulario">
                        <div class="strike">
                            <span>Buscar por:</span>
                        </div>
                        </br>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-group mb-3 col-sm-6">
                                    <label class="input-group-text" for="crop">Crop:</label>
                                    <select class="form-select me-2" id="crop" name="crop">
                                        <option value="2024">2024</option>
                                        <option value="2023">2023</option>                                     
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group mb-3 col-sm-6">
                                    <label class="input-group-text" for="opcion">Opcion:</label>
                                    <select class="form-select me-2" id="opcion" name="opcion" onchange="">
                                        <option value="0">Certificadas</option>
                                        <option value="1">Lote</option>  
                                        <option value="2">Liq ID</option>  
                                        <option value="3">Sin Liquidar</option>                                    
                                    </select>
                                </div>
                            </div>
                        </div>
                        </br>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-group mb-3 col-sm-6" id="lote">
                                    <label class="input-group-text" for="lote">Lote:</label>
                                    <input type="text" class="form-control me-4" name="lote" id="inputLote" min="0" disabled onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group mb-3 col-sm-6" id="liqId">
                                    <label class="input-group-text" for="liqId">Liq Id:</label>
                                    <input type="text" class="form-control me-4" name="liqId" id="inputLiqId" oninput="this.value = this.value.toUpperCase()" title="Por favor, ingresa letras mayúsculas" disabled>
                                </div>
                            </div>
                        </div>
                        </br></br>
                        <div class="modal-footer">
                            <button id="descargar" type="submit" class="btn btn-dark" data-toggle="modal tooltip" data-placement="bottom" title="Descargar Excel">    
                                <i class="bi bi-file-earmark-excel"></i> Descargar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </body>
    
</html>
<?php
endif;
?>