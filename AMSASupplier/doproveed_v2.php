<?php
 session_start();
if (!isset($_SESSION['proveed'])) :
    include_once('index.php');
else :
    $Gin = $_SESSION['gin'];
    $usuario=$_SESSION['proveed'];
    $LoadCertificados = $_SESSION['LoadCertificados'];
?>
    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv='cache-control' content='no-cache'>
        <meta http-equiv='expires' content='0'>
        <meta http-equiv='pragma' content='no-cache'>

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>DO PROVEED</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="main.css">
        
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>
        <!-- Scripts -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- librerias necesarias para finalizar sesion por inactividad -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>
        
        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>
        <script type="text/javascript" src="doproveed_v2.js?v=<?php echo time(); ?>"></script>

        <!-- Terminan Scripts -->
            
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>
                      
                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="#">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="purchases_v2.php">Liquidations</a></li>
                    </ul>

                    <div id="gin" style="display:none;">
                        <input id="GinID" value="<?php echo $Gin; ?>"/>
                        <input id="usuario" value="<?php echo $usuario; ?>"/>
                    </div> 
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-1 col-sm-2 col-xs-2" href="doproveed_v2.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ DO PROVEED</p>
                </div>
                <div class="container-fluid  col-xl-2 col-lg-2 col-md-1 col-sm-1 col-xs-1">
                    <!-- Botones -->
                    <button id="plan" class="btn btn-primary" title="Plan de embarque"><i class="bi bi-calendar"></i></button>
                    <button id="reporte" class="btn btn-success" title="Reporte de embarque"><i class="bi bi-list-check"></i></button>
                    <button id="manual" class="btn btn-secondary" title="Manual de Usuario"><i class="bi bi-info-circle-fill"></i></button>
                </div>
                <!--<div class="container-fluid  col-xl-2 col-lg-1 col-md-1 col-sm-1 col-xs-1 form-group row">
                    <form>
                        <select name="cars" class="form-control form-control-xs" id="idioma"  onchange="location = this.value;">
                            <option selected>language</option>
                            <option value="#">Ingles</option>
                            <option value="doproveed_esp.php">Español</option>
                            </select>
                    </form>
                </div> -->

               <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"  value="<?php echo $_SESSION['proveed']; ?>"><?php echo $_SESSION['proveed']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Close sesion</a></li>
                    </ul>
                </div>
            </div>
        </nav>
            <!-- Estilo para inmovilizar titulo de tabla secundaria -->
        
        <style type="text/css">
            .view {
            margin: auto;
            width: 200px;
            }
    
            .contenedor-tabla thead th {
            position: sticky;
            top: 0;
            background: grey;
            background-color:grey
            z-index: 1;
            border-bottom: 2px solid;
            }

            .celdaAsignado{
            word-break: break-all;
            white-space: nowrap;
            width : 150px ;
            }

            .tabla2 {
            overflow: scroll;
            white-space: nowrap;
            }

            .wrapper {
                position: relative;
                overflow: auto;
                
                white-space: nowrap;
            }

            .sticky-col {
            position: -webkit-sticky;
            position: sticky;
            background-color: white !important;
            }

            .second-col {
                width: 150px;
                min-width: 150px;
                max-width: 150px;
                left: 100px;
            }

            .fixed {
                position: absolute;
                background: #aaa;
            }
        </style>

        <!-- Estilo para cambiar el color del search de Datatabla -->
        <style>
        .dataTables_wrapper 
        .dataTables_length, div.dataTables_wrapper 
        div.dataTables_filter label, 
        div.dataTables_wrapper div.dataTables_info {
        color: white;
            
        }
        </style>
                      
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <!-- Aquí inicia todo código de tablas etc -->
        <!-- Inicio de la tabla -->
      
        <div class="card card-body " style="opacity:100%;" >
            <div class="col-xs-3">
                <div class="input-group mb-1">
                    <label class="input-group-text" >Open DO's</label>
                    <label class="input-group-text"  id="totaldo" >0</label>
                    <label class="input-group-text" >Bales to Moved</label>
                    <label class="input-group-text"  id="balesmoved" >0</label>
                </div>
            </div>
            <div class="table-responsive" style="opacity:100%;">            
                <table id="tablaClients"  class="table bg-white table-striped row-border order-column table-hover " style="opacity:100%;">
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                        <tr>
                            <th ></th>
                            <th class="th-sm">Orden de Embarque</th>
                            <th class="th-sm">Cantidad de Pacas</th>
                            <th class="th-sm">Pacas en Transito</th>
                            <th class="th-sm">Pacas por mover</th>
                            <th class="th-sm">Cosecha</th>
                            <th class="th-sm">Origen</th> 
                            <th class="th-sm">Certificadas</th> <!-- aqui -->                   
                            <th class="th-sm">Fecha OE</th>
                        </tr>
                    </thead>                        
                    <tbody></tbody>
                </table>
            </div>
            <br><br>
          
            <h6> <b><p style="color:rgb(255,255,255);">Detalle de Lotes</p></b></h6>
                
            <div class="table-responsive">
                <table id="tablatrucks"  class="table table-striped" style="width:100%">
                    <thead style="background-color: #BDC1BD;" style="opacity:100%;">
                        <tr class="table-active">
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col">Lote</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Fecha de Llegada de Camion</th>
                            <th scope="col">Hora de Llegada de Camion</th>
                            <th scope="col">ID Camion</th> 
                            <th scope="col">Estatus</th>                             
                            <th scope="col">Linea Transportista</th>
                            <th scope="col">Placas de Tractor</th>
                            <th scope="col">Placas de Caja</th>
                            <th scope="col">Nombre de Chofer</th>
                            <th scope="col">Carta Porte</th>
                            <th scope="col">O.E.</th>
                        </tr>
                    </thead>
                    <tbody id="tbody"  style="background-color: #F8FAF8;" style="opacity:100%;" class="datos"></tbody>
                </table>
            </div>
        </div>

  <!--Modal para consultar movimientos-->
        <div class="modal fade in" data-bs-backdrop="static" id="modalmovimientos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="titulomov">Datos de Movimientos</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="background-color: white;">
                        </button>
                    </div>
                    <form id="formmovimientos">
                        <div class="modal-body"> 
                            <div class="strike">
                                <span>Resumen de movimientos</span>
                            </div>                          
                            <br>
                            <div class="row">                                                           
                                <div class="input-group mb-3">                                           
                                    <label class="input-group-text" for="DepRegFil">Lotes embarcados</label>
                                    <input type="text" class="form-control form-control-sm" id="lotes_embarcados" readonly>
                                    
                                    <label class="input-group-text" for="ArrRegFil">Pacas Embarcadas</label>
                                    <input type="text" class="form-control form-control-sm" id="pacas_embarcadas" readonly> 
                                    
                                    <label class="input-group-text" for="ArrRegFil">Ordenes Embarcadas</label>
                                    <input type="text" class="form-control form-control-sm" id="ordenes_embarcadas" readonly>
                                </div>
                            </div>

                            <div class="strike">
                                <span>Fecha Reporte</span>
                            </div>
                            <br>
                            <div class="row"> 
                                <div class="input-group mb-3"> 
                                    <label class="input-group-text" for="rangofechas">Periodo</label>
                                    <select class="form-select me-2" id="rangofechas" name="rangofechas">                            
                                        <option value="0">Mes actual</option>
                                        <option value="7">Última semana</option>  
                                        <option value="30">Último mes</option>
                                        <option value="60">Últimos 2 meses</option>
                                        <option value="120">Últimos 4 meses</option>
                                        <option value="180">Últimos 6 meses</option>
                                        <option value="365">Último año</option>                                    
                                    </select>                                
                                </div>
                                <div class="input-group mb-3">
                                    <label class="input-group-text" for="fechainicio_reporte">Fecha Inicio:</label>
                                    <input type="date" class="form-control me-4" name="fechainicio_reporte" id="fechainicio_reporte">
                                    <label class="input-group-text" for="fechafin_reporte">Fecha Fin:</label>
                                    <input type="date" class="form-control me-2" name="fechafin_reporte" id="fechafin_reporte">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
                                <button id="reporte_movimientos" type="button" class="btn btn-success" data-toggle="modal tooltip" data-placement="bottom" title="Report Excel"><i class="bi bi-file-earmark-excel"></i> Descargar Reporte</button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
         <!--Modal para CRUD-->
         <div class="modal fade in" data-bs-backdrop="static" id="modalCRUD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Datos de Transporte</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="background-color: white;">
                        </button>
                    </div>
                    <form id="formtruck">
                        <div class="modal-body"> 
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">ID de Transporte</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="truckid" readonly>                                   
                                    </div>
                                </div>   

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Orden de Embarque</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="delivery" readonly>                                   
                                    </div>
                                </div>   

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Lote(s) </label>                                        
                                        <input type="text" class="form-control form-control-sm" id="lotes" readonly>                                   
                                    </div>
                                </div>   
                                <div class="col-lg-1">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Cantidad</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="cantidad" readonly>                                   
                                    </div>
                                </div>   

                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Linea Transportista</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="transporte" readonly>                                   
                                    </div>
                                </div>  
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Operador</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="operador" readonly>                                   
                                    </div>
                                </div>   

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Carta Porte</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="carta_porte" readonly>                                   
                                    </div>
                                </div>  
                            </div>
                            <div class="strike">
                                <span>Datos de Llegada de Camion</span>
                            </div>
                            <div class="row">                          
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Fecha de Llegada<font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="fechallegada" onkeydown="return false" >
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Hora de Llegada<font size=2>*</font></label>
                                        <input type="time" class="form-control form-control-sm"  id="horallegada" onkeydown="return false">
                                    </div>
                                </div> 
                            </div>
                            
                            <div class="strike">
                                <span>Datos de Salida de Camion</span>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Fecha Salida<font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="fechasalida" onkeydown="return false" >
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Hora de Salida<font size=2>*</font> </label>
                                        <input type="time" class="form-control form-control-sm" id="horasalida" onkeydown="return false" >
                                    </div>
                                </div>   

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Peso de Salida</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="pesosalida" >
                                        <span class="badge rounded-pill bg-danger" id ="alerta_peso" style="display:none">Valide el peso capturado</span>                                   
                                    </div>
                                </div>  

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Peso Promedio</label>                                        
                                        <input type="text" class="form-control form-control-sm" id="pesopromedio" readonly>                                   
                                    </div>
                                </div>                                
                            </div>
                          
                            <div class="strike">
                                <span>Informacion Adicional</span>
                            </div>

                            <div class="row"> 
                                <div class="mb-3">
                                    <label for="comentario" class="form-label">Comentarios Adicionales</label>
                                    <textarea class="form-control" id="comentarioad" rows="1"></textarea>
                                </div>
                            </div>
                           
                            <div class="row">   
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Ticket de Peso</label>
                                        
                                       <!-- <button id="update_ticket" style="display:none"  class=" btn btn-success" disabled>Actualizar Ticket</button>-->
                                        <div class="input-group mb-3">
                                            <button id="load_ticket"  class="btn btn-primary btn-sm">Cargar Ticket</button>
                                            <!--<label class="input-group-text" for="actualfile"> Waybill PDF</label> -->                                        
                                            <input  class="form-control me-2" type="text" name="actualfile" id="actualfile" placeholder="" readonly style="display:none" >           
                                            <div id="descargaticket" style="display:none"><button class="btn btn-success btn-sm viewfile" title="Descargar Ticket"><i class="material-icons">file_download</i></button>      </div>                         
                                            
                                        </div>
                                    
                                    </div>          
                                </div>
                                
                                <!-- VALIDAR SI TIENE EL PERMISO DE CARGAR LOS CERTIFICADOS-->
                                <?php if ($LoadCertificados == 1){?>      
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Certificados de Origen</label>
                                        
                                        <!-- <button id="update_ticket" style="display:none"  class=" btn btn-success" disabled>Actualizar Ticket</button>-->
                                        <div class="input-group mb-3">
                                            <button id="load_cert"  class="btn btn-primary btn-sm">Cargar Certificados</button>
                                            <!--<label class="input-group-text" for="actualfile"> Waybill PDF</label> -->                                        
                                            <input  class="form-control me-2" type="text" name="actualfile_certicados" id="actualfile_certicados" placeholder="" readonly style="display:none" >           
                                            <div id="descarga_cert" style="display:none">
                                                <button class="btn btn-success btn-sm viewfile_certificado" title="Descargar Certificados"><i class="material-icons">file_download</i></button>      
                                            </div>                         
                                            
                                        </div>
                                    
                                    </div>          
                                </div>  
                                <?php } ?>  
                                </div>

                                <div class="col-lg-3" id="documento" style="display: none">
                                    <br>
                                </div>
                            </div>   
                            <!-- LINEAS PARA SIMBOLO DE ALERTA  -->

                            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                                <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                                    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                </symbol>
                            </svg>

                             <!-- TERMINA SIMBOLO DE ALERTA  -->
                                                        
                            <div class="row" id="alerta_peso_promedio"style="display:none">
                                <div class="col-lg-12">
                                    <div class="alert alert-danger d-flex align-items-center" role="alert">
                                        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                                        <div id="mensaje">
                                            Peso invalido, ingresen peso proporcional
                                        </div>
                                    </div>
                                </div>
                            </div>
                           

                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
                                <button type="button" class="btn btn-dark" id="estatus">En Transito</button>
                                <button type="submit" id="btnGuardar" class="btn btn-dark">Guardar</button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

<!--------------------------------------------FORMULARIO PARA SUBIR Ticket de peso----------------------------------------->
        <div class="modal fade" id="modal_ticket" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ticket de Peso</h5>
                        <button id ="closeX"  class="btn-close"  aria-label="Close" style="background-color: white;">
                        </button>
                    </div>
                    <form id="formticket">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label"></label>
                                        <input type="file"  accept=".Pdf,.pdf,.JPG,.PNG" class="form-control" name="archivo" id="file" required onchange="upfile()" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div style="height: 1em;" id="avisoticket"></div>
                            <button id="load"   class="btn btn-primary" disabled>Cargar Ticket</button>
                            <!--<button id="update" style="display:none"  class=" btn btn-success" disabled>Actualizar Ticket</button> -->
                            <button id="close" class="btn btn-light" >Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--------------------------------------------FORMULARIO PARA SUBIR CERTIFICADOS----------------------------------------->
        <div class="modal fade" id="modal_certificados" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Certificados Origen</h5>
                        <button id ="closeX_cert"  class="btn-close"  aria-label="Close" style="background-color: white;">
                        </button>
                    </div>
                    <form id="formcertificado">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="" class="col-form-label"></label>
                                        <input type="file"  accept=".Pdf,.pdf,.JPG,.PNG" class="form-control" name="archivo" id="filecertificado" required onchange="upfile_certificados()" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div style="height: 1em;" id="avisocertificado"></div>
                            <button id="load_certificado"   class="btn btn-primary" disabled>Subir Certificados</button>
                            <!--<button id="update" style="display:none"  class=" btn btn-success" disabled>Actualizar Ticket</button> -->
                            <button id="close_cert" class="btn btn-light" >Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>

</html>
<?php
endif;
?>
