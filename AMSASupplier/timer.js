var idleTime = 0; 


$( document ).ready(function() {
    var t = 0;
    
    var idleInterval = setInterval(timerIncrement, 60000); //validar cada minuto si hay actividad
   

    function timerIncrement() { 
        var t = localStorage.getItem("time",idleTime)
        console.log(localStorage.getItem("time",idleTime))
        t = parseInt(t);
        idleTime = t;
        idleTime+=60;
        localStorage.setItem("time", idleTime);
        
        
        if (localStorage.getItem("time",idleTime) == (1200)) {  //debe ser una igualdad para que no quede en un loop infinito
            mensaje(); 
        } 
        
    } 
    
    $(this).mousemove(function (e) {
        
        localStorage.setItem("time", 0);
    });

    $(this).click(function (e) {
        
        localStorage.setItem("time", 0);
    });

    $(this).keypress(function (e) {
        
        localStorage.setItem("time", 0);
    });


 
});

function mensaje(){
    $.confirm({
         title: 'Log Out',
         content: 'Your session will expire in 10 seconds.',
         autoClose: 'logoutUser|10000',//tiempo que espera antes de finalizar la sesion 
         type: 'red',
         icon: 'fa fa-spinner fa-spin',
         buttons: {
             logoutUser: {
                 text: 'Log Out',
                 action: function () {
                    
                     $.alert('Session ended by inactivity');
                     salir();
                 }
             },
             continue: function () {
                 
                 reseteo();
                // $.alert('Continue to session');
             }
         }
     });


}



function reseteo(){
   
    localStorage.setItem("time",0);
}

function salir() {
    window.location.href = "logout.php"; 
    localStorage.setItem("time", 0);
}