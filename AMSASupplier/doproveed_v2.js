var DOrd;
$(document).ready(function() {
    //funcion para sumar
    jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
        return this.flatten().reduce( function ( a, b ) {
        if ( typeof a === 'string' ) {
            a = a.replace(/[^\d.-]/g, '') * 1;
        }
        if ( typeof b === 'string' ) {
            b = b.replace(/[^\d.-]/g, '') * 1;
        }
        return a + b;
        }, 0);
    });

    
    var opcion;
    opcion = 4;
    var GinID  = $.trim($('#GinID').val());
    
    tablaCli = $('#tablaClients').DataTable({

        drawCallback: function () {
            var api = this.api();
    
            var balesmoved = api.column( 4, {"filter":"applied"}).data().sum();
            var totaldo = api.rows({"filter":"applied"}).count();
           // var pesototal = api.column( 11, {"filter":"applied"}).data().sum();
            $('#balesmoved').html(balesmoved);
            $('#totaldo').html(totaldo);
           // $("#totallotes").val(numlotes);
         //   $('#totalpeso').html(pesototal);
    
        },
     
        responsive: "true",
        "order": [ 5,1, 'desc' ],
        "scrollX": false,
        "scrollY": "40vh",
        "scrollCollapse": true,
        "lengthMenu":false,
        "bPaginate": false,
        "bLengthChange": false,
        "bInfo": false,
       
        "ajax":{            
            "url": "bd/crudproveddo_v2.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion, GinID:GinID}, //enviamos opcion 4 para que haga un SELECT
            "dataSrc":""
        },
        "columns":[                                                            // quitar el sm para tamaño normal
            {"defaultContent":"<div class='text-center'><div class='btn-group btn-group-sm'><button class='btn btn-primary btn-sm btnmore'><i class='material-icons'>search</i></button><button class='btn btn-success  btn-sm btnlistado' title='Listado de pacas' ><i class='material-icons'>list</i></button><button class='btn btn-danger btn-sm btnlistadoPDF' title='Listado PDF'><i class='material-icons'>picture_as_pdf</i></button></div></div>",width: '100px'},
            {"data": "DOrd"},
            {"data": "Qty"},
            {"data": "balestransit"},
            {"data": "balespendientes"},
            {"data": "Year"},
            {"data": "GinName"},
            {"data": "Cert", // aqui 
                "render": function(data) {
                    return data == "1" ? "SI" : "NO";
                }
            }, 
            {"data": "Date"}
        ],
    });     

    var oTable = $('#tablaClients').DataTable();
    
    //MOSTRAR SUB TABLA AL DAR CLIC AL BOTÓN DE LA LUPA
    oTable.on("click", ".btnmore", function(){       
        fila = $(this).closest("tr");        
        DOrd = parseInt(fila.find('td:eq(1)').text());    
        subtabla_trucks(DOrd);  
    });

    $(document).on("click", ".btnremision", function(){
        fila = $(this);           
        Lot = $(this).closest('tr').find('td:eq(3)').text();
        DOrd = $(this).closest('tr').find('td:eq(14)').text();
        idtruck = $(this).closest('tr').find('td:eq(7)').text();

        opcion = 1;
        $.ajax({           
            "url": "bd/crudproveddo_v2.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion, DOrd:DOrd, Lot:Lot}, //enviamos opcion 4 para que haga un SELECT
            success: function(data) {
                datos2 = JSON.parse(data);
                window.open("./bd/PDFREM.php?TrkID="+idtruck+"&TrkDO="+DOrd+"&TrkTyp="+datos2[0].Tipo+"&Reg="+datos2[0].NomRegion+"&Cli="+datos2[0].NomCliente+""+"&Cert="+datos2[0].Cert);
            },
            error: function (data){
                console.log("Error al enviar datos");
            }
        });   
    });

    $(document).on("click", ".btnpdfrem", function(){
        fila = $(this);           
        idtruck = $(this).closest('tr').find('td:eq(7)').text();
        window.open("opens3.php?TrkID="+idtruck);
     
    });

    $(document).on("click", ".btndescargar", function(){
        fila = $(this);
        idtruck = $(this).closest('tr').find('td:eq(7)').text();
        window.open("./bd/descargar-cartaporte.php?idtruck="+idtruck);
    });
    
    $('#plan').on('click', function(e) {
        var GinID  = $.trim($('#GinID').val());
        //var user  = $.trim($('#usuario').val());
        e.preventDefault(); 
        window.open("./bd/plan-embarques_V2.php?user=" + GinID); 
    });

    $('#manual').on('click', function(e) {
        var GinID  = $.trim($('#GinID').val());
        //var user  = $.trim($('#usuario').val());
        e.preventDefault(); 
        window.open("./bd/manual-user-v2.php");
    });

    $(document).on("click", ".btnlistado", function(e){
        fila = $(this);           
        dor = $(this).closest('tr').find('td:eq(1)').text();   
        Cert = $(this).closest('tr').find('td:eq(7)').text();
        opcion = 2;	

        $.ajax({           
            "url": "bd/crudproveddo_v2.php", 
            "method": 'POST', //usamos el metodo POST
            //"dataType": 'json',
            "data":{opcion:opcion, dor:dor, Cert:Cert},
            success: function(data) {
                datos2 = JSON.parse(data);
                fecha = datos2[0].Date_Mail;
            // console.log(fecha);
                if(fecha =="" || fecha === null){
                    alert("Listado aun no disponible")
                }
                else{
                    e.preventDefault(); 
                    window.open("./bd/listado-pacas.php?dor="+dor + "&Cert=" + Cert);
                }
            },
            error: function (data){
                console.log("Error al enviar datos");
            }

        });  
    });

    $(document).on("click", ".btneditar", function(e){
        //e.preventDefault();
        $("#formtruck")[0].reset();
        fila = $(this);           
        idtruck = $(this).closest('tr').find('td:eq(7)').text();
        transport = $(this).closest('tr').find('td:eq(9)').text();
        cantidad = $(this).closest('tr').find('td:eq(4)').text();
        lots = $(this).closest('tr').find('td:eq(3)').text();
        delivery = $(this).closest('tr').find('td:eq(14)').text();
        fecha = $(this).closest('tr').find('td:eq(3)').text();
        statustrk = $(this).closest('tr').find('td:eq(8)').text();
        operador = $(this).closest('tr').find('td:eq(12)').text();
        carta_porte = $(this).closest('tr').find('td:eq(13)').text();
        document.getElementById('alerta_peso_promedio').style.display = 'none';

        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        //$(".modal-title").text("New Rate");
         $('#modalCRUD').modal('show');	
       // $('#modalCRUD').modal('toggle');	
        //$('#modalCRUD').modal({backdrop: 'static', keyboard: false}, 'show');
        $("#truckid").val(idtruck);
        $("#delivery").val(delivery);
        $("#lotes").val(lots);
        $("#cantidad").val(cantidad);
        $("#transporte").val(transport);
        $("#operador").val(operador);
        $("#carta_porte").val(carta_porte);
        opcion = 2;
        document.getElementById('descargaticket').style.display = 'none';
        document.getElementById('actualfile').style.display = 'none';
        $('#estatus').prop('disabled', true);
        document.getElementById('alerta_peso').style.display = 'none';
        
        //Bloquear los dias despues de la fecha actual en el calendario
        fechallegada.max = new Date().toISOString().split("T")[0];
        fechasalida.max = new Date().toISOString().split("T")[0];

        //validar peso cuando se capture
        $('#pesosalida').on( "keyup", function() {
            valida_peso($(this).val());
        });  
        
        if  (statustrk == "Transit"){
            $('#fechallegada').prop('disabled', true);
            $('#horallegada').prop('disabled', true);
            $('#fechasalida').prop('disabled', true);
            $('#horasalida').prop('disabled', true);
            $('#pesosalida').prop('disabled', true);
            $("#comentarioad").prop('disabled', true);
            $('#estatus').prop('disabled', true);
        
        }
        else if (statustrk == "Programmed"){
            $('#fechallegada').prop('disabled', false);
            $('#horallegada').prop('disabled', false);
            $('#fechasalida').prop('disabled', false);
            $('#horasalida').prop('disabled', false);
            $('#pesosalida').prop('disabled', false);
            $("#comentarioad").prop('disabled', false);
            $('#estatus').prop('disabled', false);
        }
        else {
            $('#fechallegada').prop('disabled', false);
            $('#horallegada').prop('disabled', false);
            $('#fechasalida').prop('disabled', false);
            $('#horasalida').prop('disabled', false);
            $('#pesosalida').prop('disabled', false);
            $('#estatus').prop('disabled', false);
            $("#comentarioad").prop('disabled', false);
        }

        $.ajax({
            url: "bd/crudtruck.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:opcion,truckid:idtruck},    
            success: function(data){
                opts = JSON.parse(data);
                var fecha1 = "";
                var fecha2 = "";
                //validar si ya se capturo la fecha de llegada de lo contrario se asigna la de salida
                
                $("#comentarioad").val(opts[0].comentario)

                if (opts[0].Fechallegada === null){
                    $("#fechallegada").val(opts[0].FechaSalida);
                    fecha1 =opts[0].FechaSalida;
                }                
                else {
                    $("#fechallegada").val(opts[0].Fechallegada);
                      fecha1 = opts[0].Fechallegada;
                }
                if (opts[0].Horallegada === null){
                    $("#horallegada").val(opts[0].HoraSalida);
                }
                else{
                    $("#horallegada").val(opts[0].Horallegada);
                }
                if (opts[0].FechaSalida !=""){
                    $("#fechasalida").val(opts[0].FechaSalida)
                    fecha2 = opts[0].FechaSalida;
                }
                if (opts[0].HoraSalida !=""){
                    $("#horasalida").val(opts[0].HoraSalida)
                }
                if (opts[0].PesoSalida !=""){
                    $("#pesosalida").val(separator(opts[0].PesoSalida))
                    //Funcion para validar peso al mostrar modal
                    valida_peso_modal($("#pesosalida").val(),cantidad)
                }
                if(opts[0].Fechallegada !="" && opts[0].HoraLlegada !="" &&  opts[0].FechaSalida !="" && opts[0].HoraSalida!="" && opts[0].PesoSalida != 0 && statustrk === "Programmed"){
                    $('#estatus').prop('disabled', false);
                
                }
                if (opts[0].TicketPeso === null){
                    $("#load_ticket").text("Subir Ticket");
                    document.getElementById('descargaticket').style.display = 'none';
                    document.getElementById('actualfile').style.display = 'none';                   
                }
                else{
                    $("#actualfile").val("Ticket-"+ idtruck +"."+ opts[0].TicketPeso)
                    document.getElementById('descargaticket').style.display = 'block';
                    document.getElementById('actualfile').style.display = 'block';
                    $("#load_ticket").text("Actualizar Ticket");                  
                }

                if (opts[0].FileCertificados === null){
                    $("#load_cert").text("Cargar Certificado");
                    document.getElementById('descarga_cert').style.display = 'none';
                    document.getElementById('actualfile_certicados').style.display = 'none';                   
                }
                else{
                    $("#actualfile_certicados").val("Certificado-"+ idtruck +"."+ opts[0].FileCertificados)
                    document.getElementById('descarga_cert').style.display = 'block';
                    document.getElementById('actualfile_certicados').style.display = 'block';
                    $("#load_cert").text("Actualizar Certificado");                  
                }
                $("#fechallegada").change( function() {
                    if (opts[0].Fechallegada === null){
                        resta_fecha(fecha1, $(this).val());
                       // $("#fechallegada").val(opts[0].FechaSalida);
                    }                
                    else {
                        //$("#fechallegada").val(opts[0].Fechallegada);
                        resta_fecha(fecha1, $(this).val());
                    }
                });
                $("#fechasalida").change( function() {
                    resta_fecha(fecha2, $(this).val());
                });
            },
            error: function(data) {
                alert('error');
            }
        });
    });
//MOSTRAR EL MODAL PARA CARGAR EL TICKET DE PESO 
    $('#load_ticket').on('click', function(e) {
        e.preventDefault(); 
        $("#load").text("Subir Ticket");
        $('#avisoticket').html("");
        truckid=$("#truckid").val();
        $('#load').prop('disabled', true);
        var $el = $('#file');
        $el.wrap('<form>').closest('form').get(0).reset(); //Limpiar el apartado de cargar archivo
        $el.unwrap();          
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $('#modal_ticket').modal('show');
        opcion = 4;	
        //VALIDAR SI YA SE SUBIO EL TICKET
        $.ajax({
            url: "bd/crudtruck.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:opcion,truckid:truckid},    
            success: function(data){
                opts = JSON.parse(data);             
                if (opts[0].TicketPeso === null ){
                    $("#load").text("Subir Ticket");
                }
                else
                {
                    $("#load").text("Actualizar Ticket");
                }
            },
            error: function(data) {
                alert('error');
            }
        });   
    });

    //SUBIR O ACTUALIZAR EL TICKET DE PESO 
    $('#load').on('click', function() {
        // e.preventDefault(); 
        truckid=$("#truckid").val();
        $('#load').prop('disabled', true);
        var file_data = $('#file').prop('files')[0];   
        $('#avisoticket').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
        var form_data = new FormData();             
        form_data.append('file', file_data);
        form_data.append('idtruck', truckid);
        
        $.ajax({
            url: 'LoadTicket.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(data){              
                opts = JSON.parse(data);
                $('#modal_ticket').modal('hide');
                $('#avisoticket').html("");
                alert("Ticket subido con exito"); // <-- display response from the PHP script, if any
                $("#actualfile").val(opts);
                document.getElementById('descargaticket').style.display = 'block';
                document.getElementById('actualfile').style.display = 'block';
                $("#load").text("Actualizar Ticket");
            }
            });
    });


    //MOSTRAR EL MODAL PARA CARGAR CERTIFICADOS 
    $('#load_cert').on('click', function(e) {
        e.preventDefault(); 
        $("#load_certificado").text("Cargar Certificados");
        $('#avisoticket').html("");
        truckid=$("#truckid").val();
        $('#load_certificado').prop('disabled', true);
        var $el = $('#filecertificado');
        $el.wrap('<form>').closest('form').get(0).reset(); //Limpiar el apartado de cargar archivo
        $el.unwrap();          
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $('#modal_certificados').modal('show');
        opcion = 6;	
        //VALIDAR SI YA SE SUBIERON LOS CERTIFICADOS DE ORIGEN 
        $.ajax({
            url: "bd/crudtruck.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:opcion,truckid:truckid},    
            success: function(data){
                opts = JSON.parse(data);             
                if (opts[0].FileCertificados === null ){
                    $("#load_certificado").text("Cargar Certificados");
                }
                else
                {
                    $("#load_certificado").text("Actualizar Certificados");
                }
            },
            error: function(data) {
                alert('error');
            }
        });   
    });

    //SUBIR O ACTUALIZAR CERTIFICADOS DE ORIGEN
    $('#load_certificado').on('click', function() {
        // e.preventDefault(); 
        truckid=$("#truckid").val();
        $('#load_certificado').prop('disabled', true);
        var file_data = $('#filecertificado').prop('files')[0];   
        $('#avisocertificado').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
        var form_data = new FormData();             
        form_data.append('file', file_data);
        form_data.append('idtruck', truckid);
        
        $.ajax({
            url: 'LoadCertificados.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(data){              
                opts = JSON.parse(data);
                $('#modal_ticket').modal('hide');
                $('#avisocertificado').html("");
                alert("Certificado subido con exito"); // <-- display response from the PHP script, if any
                $("#actualfile_certicados").val(opts);
                document.getElementById('descarga_cert').style.display = 'block';
                document.getElementById('actualfile_certicados').style.display = 'block';
                $("#load").text("Actualizar Ticket");
            }
        });
    });

//BOTONES PARA CERRAR MODAL DE LOAD FILE SE BORRA EL ARCHIVO QUE CARGARON
    $('#close').on('click', function(e) {
        e.preventDefault();
        var $el = $('#file');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap(); 
        $('#modal_ticket').modal('hide');	
    });

    $('#closeX').on('click', function(e) {
        e.preventDefault();
        var $el = $('#file');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap(); 
        $('#modal_ticket').modal('hide');
    });

    $('#close_cert').on('click', function(e) {
        e.preventDefault();
        var $el = $('#filecertificado');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap(); 
        $('#modal_certificados').modal('hide');	
    });

    $('#closeX_cert').on('click', function(e) {
        e.preventDefault();
        var $el = $('#filecertificado');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap(); 
        $('#modal_certificados').modal('hide');
    });

    //Actualizar los Datos del transport
    $('#formtruck').submit(function(e){
        e.preventDefault();
        opcion = 1;
        fechallegada=$("#fechallegada").val();
        horallegada=$("#horallegada").val();
        fechasalida=$("#fechasalida").val();
        horasalida=$("#horasalida").val();
        pesosalida=$("#pesosalida").val();
        truckid=$("#truckid").val();
        comentario=$("#comentarioad").val()
        pesopromedio=$("#pesopromedio").val()
        
        if(pesopromedio > 250 || pesopromedio < 190){
            document.getElementById('alerta_peso_promedio').style.display = 'block';
        }
        else {
            $.ajax({
                url: "bd/crudtruck.php",
                type: "POST",
                datatype:"json",
                data:  {opcion:opcion,fechallegada:fechallegada,horallegada:horallegada,
                        fechasalida:fechasalida,
                        horasalida:horasalida,pesosalida:pesosalida,truckid:truckid,comentario:comentario},    
                success: function(data){
                    alert(data);
                    $('#estatus').prop('disabled', false);
                    // $('#modalCRUD').modal('hide');         
                
                },
                error: function(data) {
                    alert('error');
                }
            });
        }
    });

    //Cambiar estado a Transit
    $('#estatus').on('click', function(e) {
        opcion = 3;
        fechallegada=$("#fechallegada").val();
        horallegada=$("#horallegada").val();
        fechasalida=$("#fechasalida").val();
        horasalida=$("#horasalida").val();
        pesosalida=$("#pesosalida").val();
        truckid=$("#truckid").val();
        Delivery=$("#delivery").val();
        
        if(fechallegada !="" && horallegada !="" && fechasalida !="" && horasalida!="" && pesosalida!=""){
            $.ajax({
                url: "bd/crudtruck.php",
                type: "POST",
                datatype:"json",
                data:  {opcion:opcion,truckid:truckid,Delivery:Delivery},    
                success: function(data){
                    alert(data);     
                   // $('#modalCRUD').modal('hide');	 
                    //Recargar la subtabla donde muestra los trucks  
                    subtabla_trucks(Delivery); 
                    //console.log(data)
                     if(data==='"Status Actualizado"'){
                        sendmail(truckid);
                        //console.log("ENTRO AL IF")
                    }     
                },
                error: function(data) {
                    alert('error');
                }
            });
        }
        else{
            alert("Debe llenar todos los campos");
            e.preventDefault(); 
        }
    });

    $(document).on("click", ".viewfile", function(e){
        e.preventDefault(); 
        truckid=$("#truckid").val();
        window.open("descarga_ticket.php?TrkID="+truckid);
    });

    $(document).on("click", ".viewfile_certificado", function(e){
        e.preventDefault(); 
        truckid=$("#truckid").val();
        window.open("descarga_certificado.php?TrkID="+truckid);
    });

    $("#pesosalida").on({
        "focus": function(event) {
          $(event.target).select();
        },
        "keyup": function(event) {
          $(event.target).val(function(index, value) {
            return value.replace(/\D/g, "")
              .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
          });
        }
    });
});


//BOTON PARA GENERAR LOS LISTADOS POR LOTES EN PDF
$(document).ready(function() {
    $(document).on("click", ".btnlistadoPDF", function(e){
        e.preventDefault(); 
        //fila = $(this);
        DOrd = $(this).closest('tr').find('td:eq(1)').text();
        Cert = $(this).closest('tr').find('td:eq(7)').text();
        truckid=$("#truckid").val();

        window.open("./bd/ExportListPDF.php?DOGIN=" + DOrd + "&Cert=" + Cert);
    });
});

//BOTON PARA VER RESUMEN DE LOS MOVIMIENTOS
$(document).ready(function() {
    $('#reporte').on('click', function(e) {
        GinID  = $.trim($('#GinID').val());
        e.preventDefault();          
        $(".modal-header").css("background-color", "#17562c");
        $(".modal-header").css("color", "white" );
        $('#modalmovimientos').modal('show');
        $("#formmovimientos").trigger("reset");
        var date = new Date();
        var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
        var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  
        primerDia = formatDate(primerDia);
        ultimoDia = formatDate(ultimoDia);
        $('#fechainicio_reporte').val(primerDia);
        $('#fechafin_reporte').val(ultimoDia);

        totales(GinID)
    });

    //generar reporte al dar clic al botón 
    $("#reporte_movimientos").click(function(e){ 
        fromdate=$('#fechainicio_reporte').val();
        todate=$('#fechafin_reporte').val();
        GinID  = $.trim($('#GinID').val());
        window.open("./bd/reportemovimientos.php?fechainicio="+fromdate+"&fechafin="+todate+"&iduser="+GinID);   
    });
});

//funcion que proporciona rango de fecha en Modal de movimientos
$(document).ready(function() {
    $('#rangofechas').on('change', function () {
        timeSelect = $(this).val();
        if(timeSelect){
            timeSelect = parseInt(timeSelect);
            if(timeSelect != 0){
                let today = new Date();
                //today = today.toLocaleTimeString('es-MX');
                let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
                let suma = today.getTime() - dateEnMilisegundos;
                let fechainicial = new Date(suma);
                //console.log(today);
                datetoday =  formatDate(today);
                datetosearch = formatDate(fechainicial);
                $('#fechainicio_reporte').val(datetosearch);
                $('#fechafin_reporte').val(datetoday);
            }
            else{
                var date = new Date();
                var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
                var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        
                primerDia = formatDate(primerDia);
                ultimoDia = formatDate(ultimoDia);
                $('#fechainicio_reporte').val(primerDia);
                $('#fechafin_reporte').val(ultimoDia);           
            }
        }    
    });
});


// formato miles (#,###) de manera automatica
function separator(numb) {
    var str = numb.toString().split(".");
    str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return str.join(".");
}
    
function upfile(){
    var filename = $("#file").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename === null)
        alert('No ha seleccionado una imagen');  
    else{// si se eligio un archivo correcto obtiene la extension para validarla
        var extension = filename.replace(/^.*\./, '');               
        
        //$('#update').prop('disabled', false);
        if (extension == filename)
            extension = '';
        else{
            extension = extension.toLowerCase();
          
            //aqui puedes incluir todas las extensiones que quieres permitir
            if(extension == 'pdf' || extension == 'JPG' || extension == 'PNG'|| extension == 'jpg' || extension == 'png' ){
                $('#load').prop('disabled', false);
            }
            else{
                alert("extencion no valida");
                $('#load').prop('disabled', true); 
            }
        }
    }
}

function upfile_certificados(){
    var filename = $("#filecertificado").val();
  //  console.log(filename);
    //si es null muestra un mensaje de error
    if(filename === null)
        alert('No ha seleccionado un archivo');  
    else{// si se eligio un archivo correcto obtiene la extension para validarla
        var extension = filename.replace(/^.*\./, '');               
        
        //$('#update').prop('disabled', false);
        if (extension == filename)
            extension = '';
        else{
            extension = extension.toLowerCase();
          
            //aqui puedes incluir todas las extensiones que quieres permitir
            if(extension == 'pdf' || extension == 'JPG' || extension == 'PNG'|| extension == 'jpg' || extension == 'png' ){
              
                $('#load_certificado').prop('disabled', false);
                
            }
            else{
                alert("extencion no valida");
               
                $('#load_certificado').prop('disabled', true);
            }
        }
    }
}

//Funcion para validar peso cuando se teclea el peso
function valida_peso(peso_capturado){
    peso = peso_capturado;
    cantidad = $('#cantidad').val();
    peso = peso.replace(/,/g, '');
    peso_paca = (peso/cantidad);

    if((peso_paca < 199 || peso_paca > 235) && (peso_capturado == null || peso_capturado !="") ){
    //console.log("Validar el peso capturado:" + peso_paca)
    document.getElementById('alerta_peso').style.display = 'block';
    }
    else{
    //console.log("peso correcto " + peso_paca)
    document.getElementById('alerta_peso').style.display = 'none';
    }
    $('#pesopromedio').val(peso_paca.toFixed(2));
}

//Funcion para validar peso cuando muestre el modal
function valida_peso_modal(peso,cantidad){   
    peso = peso.replace(/,/g, '');  
    peso_paca = (peso/cantidad);

    if((peso_paca <= 199 || peso_paca >= 251) && peso != 0.00){
     //  console.log("Validar el peso capturado:" + peso_paca)
       document.getElementById('alerta_peso').style.display = 'block';
    }
    else{
     //  console.log("peso correcto " + peso_paca)
       document.getElementById('alerta_peso').style.display = 'none';
    }
    $('#pesopromedio').val(peso_paca.toFixed(2));
}

//FUNCION PARA SUBTABLA AL DAR CLIC AL BOTÓN DE LA LUPA EN LA TABLA PRINCIPAL
function subtabla_trucks(DOrd){
    $('#tablatrucks').DataTable().destroy();
    $('#tablatrucks tbody').empty();
    let tableBody = document.getElementById('tbody');
    var pval="";
    opcion = 5;
    var datos1 = "";
    var transportname;
    var boton1 = '<button class="btn btn-success btn-sm btnremision" title="Generar remision"><i class="material-icons">local_shipping</i></button>';
    var boton2 = '<button class="btn btn-danger btn-sm btneditar" title="Capturar Datos"><i class="material-icons">edit</i></button>';
    var boton3 = '<button class="btn btn-primary btn-sm btndescargar" title="Descargar carta porte "><i class="material-icons">file_download</i></button>';

    $.ajax({           
        "url": "bd/crudproveddo_v2.php", 
        "method": 'POST', //usamos el metodo POST
        //"dataType": 'json',
        "data":{opcion:opcion, DOrd:DOrd}, //enviamos opcion 4 para que haga un SELECT
        success: function(data) {
        
            datos1 = JSON.parse(data);
            for (var i = 0; i< datos1.length; i++){
                transportname = datos1[i].TptCo;
                plateTruck =   datos1[i].TrkLPlt;              
                if(datos1[i].Truck!='0' && datos1[i].Datemailsent!=''){
                    if(datos1[i].RemisionPDF!='0' ){
                        pval += '<tr><td>'+boton2  +'</td><td>'+boton1+'</td><td>'+boton3 +'</td><td>'+datos1[i].Lot+'</td><td>'+datos1[i].Qty+'</td><td class="celdaAsignado">'+ datos1[i].SchOutDate+'</td><td>'+ datos1[i].SchOutTime+'</td><td>'+datos1[i].Truck+'</td><td>'+datos1[i].Status+'</td><td class="celdaAsignado">'+ transportname+'</td><td>' +
                        datos1[i].TrkLPlt+'</td><td>'+ datos1[i].TraLPlt+'</td><td class="celdaAsignado">'+datos1[i].DrvNam+ '</td><td>'+datos1[i].WBill + '</td><td>'+DOrd+ '</td></tr>';
                    }
                    else{
                        pval += '<tr><td>'+boton2  +'</td><td>'+boton1+'</td><td>'+''+'</td><td>'+datos1[i].Lot+'</td><td>'+datos1[i].Qty+'</td><td class="celdaAsignado">'+ datos1[i].SchOutDate+'</td><td>'+ datos1[i].SchOutTime+'</td><td>'+datos1[i].Truck+'</td><td>'+datos1[i].Status+'</td><td class="celdaAsignado">'+ transportname+'</td><td>' +
                        datos1[i].TrkLPlt+'</td><td>'+ datos1[i].TraLPlt+'</td><td class="celdaAsignado">'+datos1[i].DrvNam+ '</td><td>'+datos1[i].WBill + '</td><td>'+DOrd+ '</td></tr>';
                    }  
                } 
                else{
                    pval += '<tr><td >'+ '' +'</td><td >'+' ' +'</td><td >'+' ' +'</td><td>'+datos1[i].Lot+'</td><td>'+datos1[i].Qty+'</td><td class="celdaAsignado">'+ datos1[i].SchOutDate+'</td><td>'+ datos1[i].SchOutTime+'</td><td>'+datos1[i].Truck+'</td><td>'+datos1[i].Status+'</td><td class="celdaAsignado">'+ transportname+'</td><td>' +
                    datos1[i].TrkLPlt+'</td><td>'+ datos1[i].TraLPlt+'</td><td class="celdaAsignado">'+datos1[i].DrvNam+ '</td><td>'+datos1[i].WBill + '</td><td>'+DOrd+ '</td></tr>';

                }              
            }                
            tableBody.innerHTML += pval;    
        }    
    });       
}

function resta_fecha(fecha1,fecha2){
    var resta = new Date(fecha1) - new Date(fecha2);    
    var diferenciadias = resta / (1000 * 60 * 60 * 24);
    if (diferenciadias > 15){
        alert("la diferencia de días es mayor a 15 días")
    }
}

function sendmail(TrkID){
    $.ajax({
        url: "bd/correotransito.php",
        type: "POST",
        datatype:"json",
        data:  {truckid:TrkID},    
        success: function(data){        
        },
        error: function(data) {
        }
    });
}
    
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    return [year, month, day].join('-');
}

function totales(origenes){
    opcion = 5;
    $.ajax({
        url: "bd/crudtruck.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:opcion, origenen:origenes},    
            success: function(data){
            opts = JSON.parse(data);            
            $('#lotes_embarcados').val(opts[0].total_lotes);
            $('#pacas_embarcadas').val(opts[0].total_bales);
            $('#ordenes_embarcadas').val(opts[0].total_do);              
        },
        error: function(data) {
        }
    });
}