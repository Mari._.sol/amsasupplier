<?php

require './vendor/autoload.php';
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$dord = $_GET['dor'];
$Cert = $_GET['Cert'];

$consulta = 'SELECT Bales.Bal as Bal, Bales.Lot as Lot, Gines.GinName as Gin, Lots.DOrd as DO
            FROM Bales, Lots, Gines
            Where Lots.DOrd = '.$dord.' and Lots.Lot = Bales.Lot and Gines.IDGin = Lots.GinID order by Lot, Bal;';
$resultado = $conexion->prepare($consulta);
$resultado->execute();  

$siexiste=0; //$data=$resultado->fetchAll(PDO::FETCH_ASSOC);

//Renombrar el excel de acuerdo a la DO
$fileName = "Listado_de_Pacas_".$dord.".xlsx";

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Pacas");

$hojaActiva->getColumnDimension('A')->setWidth(17);
$hojaActiva->setCellValue('A1','No. Paca');
$hojaActiva->getColumnDimension('B')->setWidth(15);
$hojaActiva->setCellValue('B1','Lote');
$hojaActiva->getColumnDimension('C')->setWidth(15);
$hojaActiva->setCellValue('C1','Gin');
$hojaActiva->getColumnDimension('D')->setWidth(15);
$hojaActiva->setCellValue('D1','DO');
$hojaActiva->getColumnDimension('E')->setWidth(15);
$hojaActiva->setCellValue('E1','Certificadas');

$fila = 2;

while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
    if($siexiste==0){
        if(!empty($row['Bal']))
            $siexiste=1;
    }

    $Cert = ($Cert == "SI") ? "SI" : "NO";
    
    $hojaActiva->setCellValue('A' . $fila,$row['Bal']);
    $hojaActiva->setCellValue('B' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('C' . $fila,$row['Gin']);
    $hojaActiva->setCellValue('D' . $fila,$row['DO']);
    $hojaActiva->setCellValue('E' . $fila, $Cert);
    
    $fila++;
}


$writer = new Xlsx($excel); // Escribir un archivo .xlsx aquí va $file
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"'); //$fileName el nombre del archivo en si
$writer->save('php://output'); //Exportarlo fuera

?>