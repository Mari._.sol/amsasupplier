<?php

//setlocale(LC_TIME, "spanish");
//date_default_timezone_set('America/Mexico_City');
setlocale(LC_TIME, "spanish");

$dias_ES = array("Lunes", "Martes", "MiÉrcoles", "Jueves", "Viernes", "SÁbado", "Domingo");
$dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
$meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

//$diassemana = array("LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO", "DOMINGO");
//$meses = array("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");


$TrkID = $_GET['TrkID'];
$TrkDO = $_GET['TrkDO'];
$TrkTyp = $_GET['TrkTyp'];
$Gin = $_GET['Gin'];
$DepReg = $_GET['DepReg'];
$Reg = $_GET['Reg'];
$Cli = $_GET['Cli'];

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

if ($TrkTyp == "CON"){
    $consulta = "SELECT * FROM DOrds WHERE DOrd = '$TrkDO'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataDO=$resultado->fetch();
    $RegIn = $dataDO['InReg'];
    $RegOut = $dataDO['OutPlc'];
    
    $consulta = "SELECT * FROM Region WHERE IDReg = '$RegIn'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataRIn=$resultado->fetch();
    $CdeIn = $dataRIn['Cde'];
    
    $consulta = "SELECT * FROM Region WHERE IDReg = '$RegOut'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataROut=$resultado->fetch();
    $CdeOut = $dataROut['Cde'];
    
}else{
    $consulta = "SELECT * FROM DOrds WHERE DOrd = '$TrkDO'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataDO=$resultado->fetch();
    $InPlc = $dataDO['InPlc'];
    $RegOut = $dataDO['OutPlc'];
    
    $consulta = "SELECT * FROM Clients WHERE CliID = '$InPlc'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataRIn=$resultado->fetch();
    $CdeIn = $dataRIn['Cde'];
    
    $consulta = "SELECT * FROM Region WHERE IDReg = '$RegOut'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataROut=$resultado->fetch();
    $CdeOut = $dataROut['Cde'];
    
}


$consulta = "SELECT * FROM Truks WHERE TrkID = '$TrkID'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$data=$resultado->fetch();

$TName = $data['TNam'];

$consulta = "SELECT BnName FROM Transports WHERE TptID = '$TName'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$dataTpt=$resultado->fetch();

//Datos departure
if ($DepReg != "CLIENTE"){
    $consulta = "SELECT * FROM Region WHERE RegNam = '$DepReg'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDep=$resultado->fetch();
    $Origin = $dataDep['IsOrigin'];
    if ($Origin =="0"){
        $DepRemTo = $dataDep['BnName'];
        $DepDrc = $dataDep['Drctn'];
        $DepCP = $dataDep['CP'];
        $DepTown = $dataDep['Town'];
        $DepState = $dataDep['State'];
        $DepCt1 = $dataDep['Ct1'];
        $DepTel1 = $dataDep['Tel1'];
        $DepCt2 = $dataDep['Ct2'];
        $DepTel2 = $dataDep['Tel2'];
        $DepRef = $dataDep['Ref'];

        $Depresult1 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($DepTel1)), 2); //Dar formato tel xxx-xxx-xxxx
        $Depresult2 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($DepTel2)), 2); //Dar formato tel xxx-xxx-xxxx
    }else{
        $consulta = "SELECT * FROM Gines WHERE GinName = '$Gin'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataDep=$resultado->fetch();
        $DepRemTo = $dataDep['BnName'];
        $DepDrc = $dataDep['Drctn'];
        $DepCP = $dataDep['CP'];
        $DepTown = $dataDep['Town'];
        $DepState = $dataDep['State'];
        /*$DepCt1 = $dataDep['Ct1'];
        $DepTel1 = $dataDep['Tel1'];
        $DepCt2 = $dataDep['Ct2'];
        $DepTel2 = $dataDep['Tel2'];*/
        $DepRef = $dataDep['Ref'];

        //$Depresult1 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($DepTel1)), 2); //Dar formato tel xxx-xxx-xxxx
        //$Depresult2 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($DepTel2)), 2); //Dar formato tel xxx-xxx-xxxx
    }
}else{
    $consulta = "SELECT * FROM Clients WHERE Cli = '$Cli'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDep=$resultado->fetch();
    $DepRemTo = $dataDep['BnName'];
    $DepDrc = $dataDep['Drctn'];
    $DepCP = $dataDep['CP'];
    $DepTown = $dataDep['Town'];
    $DepState = $dataDep['State'];
    $DepRef = $dataDep['Ref'];
}

//Datos arrival
if ($Reg != "CLIENTE"){
    $consulta = "SELECT * FROM Region WHERE RegNam = '$Reg'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataCli=$resultado->fetch();
    $RemTo = $dataCli['BnName'];
    $Drc = $dataCli['Drctn'];
    $CP = $dataCli['CP'];
    $Town = $dataCli['Town'];
    $State = $dataCli['State'];
    $Ct1 = $dataCli['Ct1'];
    $Tel1 = $dataCli['Tel1'];
    $Ct2 = $dataCli['Ct2'];
    $Tel2 = $dataCli['Tel2'];
    $Ref = $dataCli['Ref'];
    
    $result1 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($Tel1)), 2); //Dar formato tel xxx-xxx-xxxx
    $result2 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($Tel2)), 2); //Dar formato tel xxx-xxx-xxxx
    
}else{
    $consulta = "SELECT * FROM Clients WHERE Cli = '$Cli'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataCli=$resultado->fetch();
    $RemTo = $dataCli['BnName'];
    $Drc = $dataCli['Drctn'];
    $CP = $dataCli['CP'];
    $Town = $dataCli['Town'];
    $State = $dataCli['State'];
    $Ref = $dataCli['Ref'];
}


include_once '../fpdf/fpdf.php';

$pdf = new FPDF();
$pdf->AddPage('portrait');
$pdf->SetTitle($TrkID);
$pdf->Ln(10);

$pdf->Cell(190,100,"",1,0,'L'); //cuadro principal
//Datos Carga
$pdf->SetFont('Arial','B',10);
$pdf->SetX($pdf->GetX() - 175);
$pdf->Cell(160,8,'Datos de carga','B',2,'L');
$pdf->Cell(20,8,'Fecha:',2,0,'L');
$pdf->SetFont('Arial','',9); 

$numeroDia = date('d', strtotime($data['OutDat']));
$dia = date('l',  strtotime($data['OutDat']));
$mes = date('F',  strtotime($data['OutDat']));
$anio = date('Y', strtotime($data['OutDat']));

$nombredia = str_replace($dias_EN, $dias_ES, $dia);
$nombreMes = str_replace($meses_EN, $meses_ES, $mes);

$fechaCompleta = $nombredia." ".$numeroDia." de ".$nombreMes." de ".$anio;

$pdf->Cell(90,8, strtoupper(utf8_decode($fechaCompleta)), 2, 0,'L');

//$pdf->Cell(90,8, strtoupper(strftime('%A, %d de %B de %Y', strtotime($data['OutDat']))), 2, 0,'L');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(15,8,'Hora: ', 2, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(35,8, substr($data['InTime'], -8, 5), 0, 0, 'L');

$pdf->Ln(5);
$pdf->SetX($pdf->GetX() - 195);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,8,'Lugar: ', 2, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(140,8, $DepRemTo, 2, 0, 'L');

$pdf->Ln(5);
$pdf->SetX($pdf->GetX() - 195);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,8, utf8_decode('Dirección: '), 2, 0, 'L');
$pdf->SetFont('Arial','', 8);
$pdf->Cell(140,8, strtoupper(utf8_decode($DepDrc)).", C.P. ".$DepCP, 2, 0, 'L');

//Datos descarga
$pdf->Ln(12);
$pdf->SetX($pdf->GetX() - 195);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(160,8,'Datos de descarga','B',2,'L');
$pdf->Cell(20,8,'Fecha:',2,0,'L');
$pdf->SetFont('Arial','',9);

$numeroDia = date('d', strtotime($data['InDat']));
$dia = date('l',  strtotime($data['InDat']));
$mes = date('F',  strtotime($data['InDat']));
$anio = date('Y', strtotime($data['InDat']));

$nombredia = str_replace($dias_EN, $dias_ES, $dia);
$nombreMes = str_replace($meses_EN, $meses_ES, $mes);

$fechaCompleta = $nombredia." ".$numeroDia." de ".$nombreMes." de ".$anio;

$pdf->Cell(90,8, strtoupper(utf8_decode($fechaCompleta)), 2, 0,'L');


//$pdf->Cell(90,8, strtoupper(strftime('%A, %d de %B de %Y', strtotime($data['InDat']))), 2, 0,'L');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(15,8,'Hora: ', 2, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(35,8, substr($data['InTime'], -8, 5), 0, 0, 'L');

$pdf->Ln(5);
$pdf->SetX($pdf->GetX() - 195);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,8,'Lugar: ', 2, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(140,8, $RemTo, 2, 0, 'L');

$pdf->Ln(5);
$pdf->SetX($pdf->GetX() - 195);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,8, utf8_decode('Dirección: '), 2, 0, 'L');
$pdf->SetFont('Arial','', 8);
$pdf->Cell(140,8, strtoupper(utf8_decode($Drc)).", C.P. ".$CP, 2, 0, 'L');

$pdf->Ln(5);
$pdf->SetX($pdf->GetX() - 195);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,8,'Referencia: ', 2, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(140, 8, utf8_decode($Ref), 2, 0, 'L');

if ($Reg != "CLIENTE"){
$pdf->Ln(12);
$pdf->SetX($pdf->GetX() - 195);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(160,8,'Datos de contacto','B',2,'L');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,8, 'Contacto: ', 2, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(140,8, 'Christian Becerra', 2, 0, 'L');

$pdf->Ln(5);
$pdf->SetX($pdf->GetX() - 195);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,8, utf8_decode('Teléfono: '), 2, 0, 'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(140,8, '222-650-0994', 2, 0, 'L');
} else {
    if ($State == "PUEBLA"){
        $pdf->Ln(12);
        $pdf->SetX($pdf->GetX() - 195);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(160,8,'Datos de contacto','B',2,'L');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,8, 'Contacto: ', 2, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(140,8, 'Christian Becerra', 2, 0, 'L');

        $pdf->Ln(5);
        $pdf->SetX($pdf->GetX() - 195);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,8, utf8_decode('Teléfono: '), 2, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(140,8, '222-650-0994', 2, 0, 'L');
    }else{
        $pdf->Ln(12);
        $pdf->SetX($pdf->GetX() - 195);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(160,8,'Datos de contacto','B',2,'L');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,8, 'Contacto: ', 2, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(140,8, 'Bruno Barrientos', 2, 0, 'L');

        $pdf->Ln(5);
        $pdf->SetX($pdf->GetX() - 195);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,8, utf8_decode('Teléfono: '), 2, 0, 'L');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(140,8, '618-134-0710', 2, 0, 'L');
    }
}
$pdf->Output('I', $TrkDO);

$conexion=null;

?>
