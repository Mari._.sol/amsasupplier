<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set('America/Los_Angeles');
setlocale(LC_TIME, "spanish");

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$fechallegada = (isset($_POST['fechallegada'])) ? $_POST['fechallegada'] : '';
$horallegada = (isset($_POST['horallegada'])) ? $_POST['horallegada'] : '';
$fechacarga = (isset($_POST['fechacarga'])) ? $_POST['fechacarga'] : '';
$horacarga = (isset($_POST['horacarga'])) ? $_POST['horacarga'] : '';
$fechasalida = (isset($_POST['fechasalida'])) ? $_POST['fechasalida'] : '';
$horasalida = (isset($_POST['horasalida'])) ? $_POST['horasalida'] : '';
$pesosalida = (isset($_POST['pesosalida'])) ? $_POST['pesosalida'] : '';
$truckid= (isset($_POST['truckid'])) ? $_POST['truckid'] : '';
$DOrd = (isset($_POST['Delivery'])) ? $_POST['Delivery'] : '';
$comentario = (isset($_POST['comentario'])) ? $_POST['comentario'] : '';

$orignes = (isset($_POST['origenen'])) ? $_POST['origenen'] : '';

switch($opcion){
    case 1:
        $consulta="";
        if ($fechallegada !=""){
            $consulta .= "UPDATE Truks SET InGinDate ='$fechallegada' WHERE TrkID='$truckid';";       
        }
        if ($horallegada !=""){
            $consulta .= "UPDATE Truks SET InGinTime='$horallegada' WHERE TrkID='$truckid';";	       
        }
        
        if ($fechasalida !=""){
            $consulta .= "UPDATE Truks SET OutDat='$fechasalida' WHERE TrkID='$truckid';";       
        }
        if ($horasalida !=""){
            $consulta .= "UPDATE Truks SET OutTime='$horasalida' WHERE TrkID='$truckid';";   
        }

        if ($pesosalida !=""){
            $peso = str_replace(",","",$pesosalida);
            $consulta .= "UPDATE Truks SET OutWgh='$peso' WHERE TrkID='$truckid';";   
        }
        if($comentario !=""){
            $comentario=mb_strtoupper($comentario, 'UTF-8');
            $consulta .= "UPDATE Truks SET ComentarioProveed='$comentario' WHERE TrkID='$truckid';";   
        }

        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data ="Datos Actualizados";


    break;

    case 2:
        $consulta = "SELECT OutDat AS FechaSalida,OutTime AS HoraSalida,OutWgh AS PesoSalida,TicketPeso, InGinDate  as Fechallegada,InGinTime as Horallegada,ComentarioProveed as comentario,FileCertificados From Truks  WHERE TrkID='$truckid';";  
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);

    break;


    case 3:
        $fecha_actual = date("d-m-Y");
        $consulta = "Select InGinDate  AS FechaLlegada,InGinTime AS HoraLlegada,OutDat AS FechaSalida,OutTime AS HoraSalida,OutWgh AS PesoSalida,TicketPeso,FileCertificados From Truks  WHERE TrkID='$truckid';";  
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $datos = $resultado->fetch();       
        
        $consulta2="select Clients.TicketPesoCli 
from amsadb1.DOrds, amsadb1.Clients, amsadb1.Truks
where DOrds.InReg = 99001 and DOrds.InPlc != 0 and Truks.DO = DOrds.DOrd  and DOrds.InPlc = Clients.CliID and  TrkID='$truckid' ";
        $resultado2 = $conexion->prepare($consulta2);
        $resultado2->execute();
        $requiereticket = $resultado2->fetch();  

      //Validar si se requiere el ticket de peso
      if ($requiereticket['TicketPesoCli'] == 1 && ($datos['TicketPeso']== null || $datos['TicketPeso']== "") ){
      
        $bandera1= 1;
      }
      else{
        $bandera1 = 0;
      }
      
                                                            


    if ($datos['FechaLlegada'] !="" && $datos['HoraLlegada']!=""  && $datos['FechaSalida']!="" && $datos['HoraSalida']!="" && $datos['PesoSalida']!=0 ){
        
          if( $bandera1== 0){
              $consulta = "UPDATE Truks SET Status='Transit' WHERE TrkID='$truckid';";  
              $resultado = $conexion->prepare($consulta);
              $resultado->execute();
              $data="Status Actualizado";
              
              $consulta = "UPDATE DOrds SET Rcv = (SELECT SUM(CrgQty) FROM Truks WHERE DO = '$DOrd' and Status = 'Received'), Tran = (SELECT SUM(CrgQty)                           FROM Truks WHERE DO = '$DOrd' and Status = 'Transit')  WHERE DOrd = '$DOrd'";       
              $resultado = $conexion->prepare($consulta);
              $resultado->execute();
              
          }
          else{
            $data = "Se requiere que suba el ticket de peso";
          }
         
    
    }
    else{
            $data="Asegurese de guardar los datos";
    }

    break;


    
    case 4:
        $consulta = "SELECT TicketPeso From Truks WHERE TrkID='$truckid';";  
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    
       case 5;
        $consulta ="select count(Lots.Lot) as total_lotes , sum(Lots.Qty) as total_bales,  count(distinct(DOrds.DOrd)) as total_do
        From amsadb1.Lots
            LEFT JOIN amsadb1.Truks
            ON Lots.TrkID = Truks.TrkID
            LEFT JOIN amsadb1.DOrds
            ON  Lots.DOrd = DOrds.DOrd
            LEFT JOIN amsadb1.Gines
            ON  Lots.GinID=Gines.IDGin 
            LEFT JOIN amsadb1.Region
            ON  DOrds.OutPlc=Region.IDReg 
            LEFT JOIN amsadb1.Region R1
            ON  DOrds.InReg=R1.IDReg 
        
        where Truks.Status IN ('Transit','Received') and Region.IsOrigin = 1 and DOrds.Gin IN (".$orignes.") and Lots.Crop = 2024 ;";

        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;

    case 6:
        $consulta = "SELECT FileCertificados From Truks WHERE TrkID='$truckid';";  
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;


?>