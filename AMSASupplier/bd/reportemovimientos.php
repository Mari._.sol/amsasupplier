<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
date_default_timezone_set("America/Mexico_City");
// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill, Alignment};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment;
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;




$fechainicio = (!empty($_GET['fechainicio'])) ? $_GET['fechainicio'] : '';
$fechafin = (!empty($_GET['fechafin'])) ? $_GET['fechafin'] : '';
$origen = (!empty($_GET['iduser'])) ? $_GET['iduser'] : '';


$consulta="SELECT Lots.Lot,Lots.Qty,Lots.DOrd,Lots.TrkID,Gines.GinName,Truks.OutDat,Truks.OutTime, ROUND(((Truks.OutWgh/Truks.CrgQty)*Lots.Qty),2) as peso_salida,
if(Truks.TicketPeso is not NULL, 'SI', 'NO') as ticketpeso
From amsadb1.Lots
    LEFT JOIN amsadb1.Truks
    ON Lots.TrkID = Truks.TrkID
    LEFT JOIN amsadb1.DOrds
    ON  Lots.DOrd = DOrds.DOrd
    LEFT JOIN amsadb1.Gines
    ON  Lots.GinID=Gines.IDGin 
    LEFT JOIN amsadb1.Region
    ON  DOrds.OutPlc=Region.IDReg 
    LEFT JOIN amsadb1.Region R1
    ON  DOrds.InReg=R1.IDReg 
    left join amsadb1.Transports
    ON Truks.TNam = Transports.TptID

where Truks.Status IN ('Transit','Received') and Region.IsOrigin = 1 and DOrds.Gin IN (".$origen.") and Lots.Crop = 2024  and   (Truks.OutDat BETWEEN '$fechainicio' AND '$fechafin') ORDER BY Truks.OutDat,Truks.OutTime asc;";
//print_r($consulta);
    




$fileName = "Reporte Embarques del ".date('d-m-Y', strtotime($fechainicio))." al ".date('d-m-Y', strtotime($fechafin)).".xlsx";
$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Embarques");



$hojaActiva->getColumnDimension('A')->setWidth(12);
$hojaActiva->setCellValue('A1','Lote');
$hojaActiva->getColumnDimension('B')->setWidth(12);
$hojaActiva->setCellValue('B1','Pacas');
$hojaActiva->getColumnDimension('C')->setWidth(20);
$hojaActiva->setCellValue('C1','Orden de embarque');
$hojaActiva->getColumnDimension('D')->setWidth(12);
$hojaActiva->setCellValue('D1','TruckID');
$hojaActiva->getColumnDimension('E')->setWidth(12);
$hojaActiva->setCellValue('E1','Planta');
$hojaActiva->getColumnDimension('F')->setWidth(12);
$hojaActiva->setCellValue('F1','Fecha Salida');
$hojaActiva->getColumnDimension('G')->setWidth(12);
$hojaActiva->setCellValue('G1','Hora Salida');
$hojaActiva->getColumnDimension('H')->setWidth(12);
$hojaActiva->setCellValue('H1','Peso Salida');
$hojaActiva->getColumnDimension('I')->setWidth(14);
$hojaActiva->setCellValue('I1','Ticket de Peso');

//Negritas en el encabezado
$hojaActiva->getStyle('A1:I1')->getFont()->setBold( true );

//relleno de celda encabezado
$hojaActiva->getStyle('A1:I1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFE1E1E1');

//frezear la primer fila
$hojaActiva->freezePane('A2');


$fila = 2;
$resultado = $conexion->prepare($consulta);
$resultado->execute();  
while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
    
    //formato de fecha al resultado de la consulta
    $fechasal = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel( $row['OutDat'] );
    //FORMATO DE FECHA EN LA COLUMNA
    $hojaActiva->getStyle('F' . $fila)->getNumberFormat()//formato de fecha 
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

    $hojaActiva->setCellValue('A' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('B' . $fila,$row['Qty']);
    $hojaActiva->setCellValue('C' . $fila,$row['DOrd']);
    $hojaActiva->setCellValue('D' . $fila, $row['TrkID']);
    $hojaActiva->setCellValue('E' . $fila, $row['GinName']);
    $hojaActiva->setCellValue('F' . $fila,$fechasal);
    $hojaActiva->setCellValue('G' . $fila,$row['OutTime']);   
    $hojaActiva->setCellValue('H'. $fila,$row['peso_salida']);
    $hojaActiva->setCellValue('I'. $fila,$row['ticketpeso']);

    
    $fila++;
}



header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;

