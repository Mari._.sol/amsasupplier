<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$GinID = (isset($_POST['GinID'])) ? $_POST['GinID'] : '';
$usuario = (isset($_POST['usuario'])) ? $_POST['usuario'] : '';
$liq = (isset($_POST['liq'])) ? $_POST['liq'] : '';
$tipoLiq = (isset($_POST['tipoLiq'])) ? $_POST['tipoLiq'] : '';

$supp = substr($liq, 3, 3);


switch ($opcion) {
    case 1:
        $consulta = "
            SELECT 
                IdLiq,
                Crop,
                prov,
                Proveedor,
                Contrato,
                LiqID,
                Pacas,
                TipoPago,
                TipoLiq,
                CASE 
                    WHEN prov = 1 THEN Precio
                    ELSE '0' 
                END AS Precio,
                CASE 
                    WHEN prov = 1 THEN PesoProm
                    ELSE '' 
                END AS PesoProm,
                CASE 
                    WHEN prov = 1 THEN payMetod
                    ELSE '' 
                END AS payMetod,
                CASE 
                    WHEN prov = 1 THEN datProv
                    ELSE '' 
                END AS datProv,
                CASE 
                    WHEN prov = 1 THEN ConDescuento
                    ELSE '0' 
                END AS ConDescuento,
                CASE 
                    WHEN prov = 1 THEN Prom
                    ELSE '0' 
                END AS Prom,
                CASE 
                    WHEN prov = 1 THEN fileFactura
                    ELSE '' 
                END AS fileFactura,
                CASE 
                    WHEN prov = 1 THEN filePago
                    ELSE '' 
                END AS filePago,
                CASE 
                    WHEN prov = 1 THEN pdfComp
                    ELSE '' 
                END AS pdfComp,
                CASE 
                    WHEN prov = 1 THEN prov
                    ELSE '' 
                END AS acciones
            FROM amsadb1.Liquidation 
            WHERE supplier = (
                SELECT supplier 
                FROM amsadb1.proveed 
                WHERE ProveedID = :usuario
            )";
        
        $resultado = $conexion->prepare($consulta);
        $resultado->bindParam(':usuario', $usuario);
        $resultado->execute();
        $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 2:
        $consulta = "SELECT fileFactura, fileXML, pdfComp, xmlComp FROM amsadb1.Liquidation WHERE LiqID='$liq' and TipoLiq='$tipoLiq';";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
}
print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;