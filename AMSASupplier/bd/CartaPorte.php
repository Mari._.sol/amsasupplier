<?php

setlocale(LC_TIME, "spanish");

$dias_ES = array("Lunes", "Martes", "MiÉrcoles", "Jueves", "Viernes", "SÁbado", "Domingo");
$dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
$meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

$TrkID = $_GET['TrkID'];
$TrkDO = $_GET['TrkDO'];
$TrkTyp = $_GET['TrkTyp'];
$TrkQty = $_GET['TrkQty'];
$Gin = $_GET['Gin'];
$DepReg = $_GET['DepReg'];
$Reg = $_GET['Reg'];
$Cli = $_GET['Cli'];

$WghNet = $TrkQty*220; 

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

if ($TrkTyp == "CON"){
    $consulta = "SELECT * FROM DOrds WHERE DOrd = '$TrkDO'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataDO=$resultado->fetch();
    $RegIn = $dataDO['InReg'];
    $RegOut = $dataDO['OutPlc'];
    
    $consulta = "SELECT * FROM Region WHERE IDReg = '$RegIn'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataRIn=$resultado->fetch();
    $CdeIn = $dataRIn['Cde'];
    
    $consulta = "SELECT * FROM Region WHERE IDReg = '$RegOut'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataROut=$resultado->fetch();
    $CdeOut = $dataROut['Cde'];
    
}else{
    $consulta = "SELECT * FROM DOrds WHERE DOrd = '$TrkDO'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataDO=$resultado->fetch();
    $Ctc = $dataDO['Ctc'];
    $InPlc = $dataDO['InPlc'];
    $RegOut = $dataDO['OutPlc'];
    
    $consulta = "SELECT * FROM Clients WHERE CliID = '$InPlc'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataRIn=$resultado->fetch();
    $CdeIn = $dataRIn['Cde'];
    
    $consulta = "SELECT * FROM Region WHERE IDReg = '$RegOut'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataROut=$resultado->fetch();
    $CdeOut = $dataROut['Cde'];
    
}


$consulta = "SELECT * FROM Truks WHERE TrkID = '$TrkID'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$data=$resultado->fetch();

$TName = $data['TNam'];
$LotsAssoc = $data['LotsAssc'];
$LotesA = str_replace("|", "-", $LotsAssoc);

$consulta = "SELECT BnName FROM Transports WHERE TptID = '$TName'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$dataTpt=$resultado->fetch();

//Datos departure
if ($DepReg != "CLIENTE"){
    $consulta = "SELECT * FROM Region WHERE RegNam = '$DepReg'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDep=$resultado->fetch();
    $Origin = $dataDep['IsOrigin'];
    if ($Origin =="0"){
        $ID = $dataDep["IDReg"];
        $DepNam = $dataDep["RegNam"];
        $DepRemTo = $dataDep['BnName'];
	$DepRFC = $dataDep['RFC'];
        $DepDrc = $dataDep['Drctn'];
        $DepCP = $dataDep['CP'];
        $DepCty = $dataDep['Cty'];
        $DepTown = $dataDep['Town'];
        $DepState = $dataDep['State'];
        $DepCt1 = $dataDep['Ct1'];
        $DepTel1 = $dataDep['Tel1'];
        $DepCt2 = $dataDep['Ct2'];
        $DepTel2 = $dataDep['Tel2'];
        $DepRef = $dataDep['Ref'];

        $Depresult1 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($DepTel1)), 2); //Dar formato tel xxx-xxx-xxxx
        $Depresult2 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($DepTel2)), 2); //Dar formato tel xxx-xxx-xxxx
    }else{
        $consulta = "SELECT * FROM Gines WHERE GinName = '$Gin'"; 
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $dataDep=$resultado->fetch();
        $ID = $dataDep["IDGin"];
        $DepNam = $dataDep["GinName"];
        $DepRemTo = $dataDep['BnName'];
	$DepRFC = $dataDep['RFC'];
        $DepDrc = $dataDep['Drctn'];
        $DepCP = $dataDep['CP'];
        $DepCty = $dataDep['Cty'];
        $DepTown = $dataDep['Town'];
        $DepState = $dataDep['State'];
        /*$DepCt1 = $dataDep['Ct1'];
        $DepTel1 = $dataDep['Tel1'];
        $DepCt2 = $dataDep['Ct2'];
        $DepTel2 = $dataDep['Tel2'];*/
        $DepRef = $dataDep['Ref'];

        //$Depresult1 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($DepTel1)), 2); //Dar formato tel xxx-xxx-xxxx
        //$Depresult2 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($DepTel2)), 2); //Dar formato tel xxx-xxx-xxxx
    }
}else{
    $consulta = "SELECT * FROM Clients WHERE Cli = '$Cli'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataDep=$resultado->fetch();
    $DepRemTo = $dataDep['BnName'];
    $DepRFC = $dataDep['RFC'];
    $DepDrc = $dataDep['Drctn'];
    $DepCP = $dataDep['CP'];
    $DepTown = $dataDep['Town'];
    $DepState = $dataDep['State'];
    $DepRef = $dataDep['Ref'];
}

//Datos arrival
if ($Reg != "CLIENTE"){
    $consulta = "SELECT * FROM Region WHERE RegNam = '$Reg'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataCli=$resultado->fetch();
    $IDTo = $dataCli["IDReg"];
    $RemNam = $dataCli['RegNam'];
    $RemRFC = $dataCli['RFC']; 
    $RemTo = $dataCli['BnName'];
    $Drc = $dataCli['Drctn'];
    $CP = $dataCli['CP'];
    $CtyTo = $dataCli['Cty'];
    $Town = $dataCli['Town'];
    $State = $dataCli['State'];
    $Ct1 = $dataCli['Ct1'];
    $Tel1 = $dataCli['Tel1'];
    $Ct2 = $dataCli['Ct2'];
    $Tel2 = $dataCli['Tel2'];
    $Ref = $dataCli['Ref'];
    
    $result1 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($Tel1)), 2); //Dar formato tel xxx-xxx-xxxx
    $result2 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($Tel2)), 2); //Dar formato tel xxx-xxx-xxxx
    
}else{
    $consulta = "SELECT * FROM Clients WHERE Cli = '$Cli'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataCli=$resultado->fetch();
    $IDTo = $dataCli["CliID"];
    $RemNam = $dataCli['Cli'];
    $RemTo = $dataCli['BnName'];
    $RemRFC = $dataCli['RFC'];
    $Drc = $dataCli['Drctn'];
    $CP = $dataCli['CP'];
    $CtyTo = $dataCli['Cty'];
    $Town = $dataCli['Town'];
    $State = $dataCli['State'];
    $Ref = $dataCli['Ref'];
}

include_once '../fpdf/fpdf.php';

$pdf = new FPDF();
$pdf->AddPage('portrait');
$pdf->SetTitle($TrkID);
$pdf->SetFont('Arial','B',10);
$pdf->Image('../img/logo1.png', 15, 10, 20, 20, 'PNG');
$pdf->Cell(0,10, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0,10,'COTTON DIVISION', 0, 0,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial','',8);
//$pdf->Cell(0,10,'Calzada de los Forjadores No. 1250, Brittingham, Gomez Palacio, Durango, C.P. 35030, Tel. (871)7145304', 0, 0,'C');

$pdf->Ln(8);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0,10,'COMPLEMENTO DE CARTA PORTE', 0, 0,'C');
//CUERPO REMISION
//----------------------- || Origen ||-------------------------------------------------------------
$pdf->Ln(10);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Tipo de Ubicacion: ', 'T', 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, "Origen", 'T', 0, 'L'); //buscar en tabla Reg o Client dependiendo de CON, DOM, Exp
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Fecha de salida: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(37, 8, strtoupper(strftime('%d/%b/%Y', strtotime($data['OutDat']))), 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Hora de salida: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, $data['OutTime'], 0, 0, 'C');

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Tipo de estacion: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, "Origen Nacional", 0, 0, 'L');

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Clave de estacion: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(37, 8, "", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Clave Transporte: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, "", 0, 0, 'C');

$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'ID Origen: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(28, 8, $ID, 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(30,8,'Salida Origen: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(28, 8, strtoupper($DepNam), 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Residencia Fiscal: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, $DepCty, 0, 0, 'L');
$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Nombre del remitente: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, $DepRemTo, 0, 0, 'L');

if ($DepCty != "MEXICO"){
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(35,8,'NIT: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, "", 0, 0, 'L');
}else{
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(35,8,'RFC: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, $DepRFC, 0, 0, 'L');
}

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Domicilio: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, strtoupper(utf8_decode($DepDrc)).", C.P. ".$DepCP, 0, 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, strtoupper(utf8_decode($DepTown)).", ".$DepState, 0, 0, 'L');


//------------------------------- || Destino || ------------------------------------------------------
$pdf->Ln(10);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Tipo de Ubicacion: ', 'T', 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, "Destino", 'T', 0, 'L'); //buscar en tabla Reg o Client dependiendo de CON, DOM, Exp
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Fecha de llegada: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(37, 8, strtoupper(strftime('%d/%b/%Y', strtotime($data['InDat']))), 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Hora de llegada: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, $data['InTime'], 0, 0, 'C');

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Tipo de estacion: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, "Destino Final Nacional", 0, 0, 'L');

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Clave de estacion: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(37, 8, "", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Clave Transporte: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, "", 0, 0, 'C');

$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'ID Destino: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(12, 8, $IDTo, 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(23,8,'Destinatario: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(53, 8, strtoupper($RemNam), 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(32,8,'Residencia Fiscal: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, $CtyTo, 0, 0, 'L');
$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Nombre del remitente: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, $RemTo, 0, 0, 'L');

if ($CtyTo != "MEXICO"){
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(35,8,'NIT: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, "", 0, 0, 'L');
}else{
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(35,8,'RFC: ', 0, 0,'L');
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(0, 8, $RemRFC, 0, 0, 'L');
}

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Domicilio: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, strtoupper(utf8_decode($Drc)).", C.P. ".$CP, 0, 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, strtoupper(utf8_decode($Town)).", ".$State, 0, 0, 'L');



$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Distancia recorrida: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(50, 8, "", 0, 0, 'L'); //Valor prov
if ($TrkTyp != "CON"){
$pdf->SetFont('Arial','B',9);
$pdf->Cell(30,8,'Contrato: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, strtoupper($Ctc), 0, 0, 'L');
}
//------------------------------- || Mercancia || ------------------------------------------------------
$pdf->Ln(10);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0,8,'Mercancia', 'T', 0,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(33,8,'Peso bruto total: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(20, 8, "Kg", 0, 0, 'L'); //valor prov
$pdf->SetFont('Arial','B',9);
$pdf->Cell(36,8,'Peso neto total: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(18, 8, $WghNet, 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(40,8,'Num. total de mercancia: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, $TrkQty, 0, 0, 'L');

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(33,8,'Unidad de peso: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(20, 8, "XBL", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(36,8,'Bienes transportados: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(18, 8, "11151507", 0, 0, 'L'); //valor prov
$pdf->SetFont('Arial','B',9);
$pdf->Cell(40,8,utf8_decode('Descripción: '), 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, utf8_decode('Fibras de algodón'), 0, 0, 'L');

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(33,8,'Clave STCC: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(20, 8, "0112", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(36,8,'Clave unidad: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(18, 8, "", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(40,8,'Material peligroso: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, "No", 0, 0, 'L'); //valor prov

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(33,8,'Peso en kilogramos: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(20, 8, $WghNet, 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(36,8,'Valor de la mercancia: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(18, 8, $TrkQty*15000, 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(40,8,'Moneda: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, "MXN", 0, 0, 'L');

//------------------------------- || Autotransporte || ------------------------------------------------------
$pdf->Ln(10);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0,8,'Autotransporte', 'T', 0,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(27,8,'Permiso SCT: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(20, 8, "", 0, 0, 'L'); //valor prov
$pdf->SetFont('Arial','B',9);
$pdf->Cell(31,8,'Num. permiso SCT: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(21, 8, "", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(28,8,'Config. Vehicular: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, "", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(17,8,'Placa VM: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, "XBL1234", 0, 0, 'L');

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(27,8, utf8_decode('Año Modelo VM: '), 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(20, 8, "", 0, 0, 'L'); //valor prov
$pdf->SetFont('Arial','B',9);
$pdf->Cell(31,8, "Asegura Resp Civil: ", 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(21, 8, "", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(28,8,'Poliza Resp Civil: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, "", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(17,8,'Placa: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, "", 0, 0, 'L');

//------------------------------- || Cliente || ------------------------------------------------------
$pdf->Ln(10);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0,8,'Cliente', 'T', 0,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, utf8_decode("Agroindustrias Unidas de México S.A. de C.V."), 0, 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, utf8_decode("RFC: AUM980109Q78"), 0, 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','',9);
$pdf->MultiCell(0, 8, utf8_decode('Bosques de Aliso 45A 2° Piso, Col. Bosque de las Lomas, Del. Cuajimalpa de Morelos, C.P. 05120, Ciudad de México, México.'),  0, 'L', false);

//------------------------------- || Comprobante || ------------------------------------------------------
$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0, 8,'Comprobante', 'T', 0,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(27, 8,'Subtotal: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(20, 8, "", 0, 0, 'L'); //valor prov
$pdf->SetFont('Arial','B',9);
$pdf->Cell(31,8,'Moneda: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(21, 8, "", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(28,8,'Total: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, "", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(22,8,'Uso del CFDI: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, "G03", 0, 0, 'L');

//------------------------------- || Carta Porte || ------------------------------------------------------
$pdf->Ln(10);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0,8,'Carta Porte', 'T', 0,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(27,8,'Version: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(20, 8, "2.0", 0, 0, 'L'); //valor prov
$pdf->SetFont('Arial','B',9);
$pdf->Cell(40,8,'Transporte Internacional: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, "No", 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(41,8,'Total Distancia Recorrida: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, "", 0, 0, 'L');


$pdf->Output('I', "Etiqueta ".$LotesA.".pdf");

$conexion=null;

?>
