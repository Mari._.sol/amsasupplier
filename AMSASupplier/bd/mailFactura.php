<?php
  require '../vendor/autoload.php';
  
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMTP;
  use Aws\S3\S3Client; 
  use Aws\Exception\AwsException;

  include_once '../bd/conexion.php';
  $objeto = new Conexion();
  $conexion = $objeto->Conectar();

  require './vendor/phpmailer/phpmailer/src/PHPMailer.php';
  require './vendor/phpmailer/phpmailer/src/SMTP.php';
  require './vendor/autoload.php';
  date_default_timezone_set('America/Los_Angeles');
  
    $usu = (isset($_POST['usuario'])) ? $_POST['usuario'] : '';
    $LiqID = (isset($_POST['LiqID'])) ? $_POST['LiqID'] : '';
    $prov = (isset($_POST['prov'])) ? $_POST['prov'] : '';
    $subject = (isset($_POST['subject'])) ? $_POST['subject'] : '';
    $contrato = (isset($_POST['contrato'])) ? $_POST['contrato'] : '';
    $pacas = (isset($_POST['pacas'])) ? $_POST['pacas'] : '';

    $bucket = 'pruebasportal'; // bucket de pruebas
    //$bucket = 'portal-liq';
    $keyname = 'Fac-'.$LiqID.'.pdf';
    $filepath = './temp/'.$keyname;

    $keyname1 = "Fac-".$LiqID.".xml";
    $filepath1 = './temp/'.$keyname1;

  $s3 = new S3Client([
    'version'     => 'latest',
    'region'      => 'us-east-2', //bucket de pruebas
    //'region'      => 'us-east-1',
    'credentials' => [
        'key'    => 'AKIAT442VUCJQXPEFVZW',
        'secret' => '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',
    ],
  ]);
  try {
    $result = $s3->getObject([
        'Bucket' => $bucket,
        'Key'    => $keyname,
        'SaveAs' => $filepath
    ]);
  }
  catch (S3Exception $e) {
      echo $e->getMessage() . PHP_EOL;
  }
  try {
      $result = $s3->getObject([
          'Bucket' => $bucket,
          'Key'    => $keyname1,
          'SaveAs' => $filepath1
      ]);
  }
  catch (S3Exception $e) {
      echo $e->getMessage() . PHP_EOL;
  }

  

  $query = 'SELECT User, email, passApp FROM amsadb1.Users WHERE User = "MARISOL HERNANDEZ GARCIA"';
  $result =$conexion->prepare($query);
  $result->execute();
  $datosuser=$result->fetch(PDO::FETCH_ASSOC);

  $email=$datosuser['email'];
  $pass =$datosuser['passApp'];
  $usuario = ucwords(strtolower($datosuser['User']));



    // Intancia de PHPMailer
    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0; 
    $mail->Host = 'smtp.gmail.com';
    $mail->Port  = 465; 
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; 
    $mail->SMTPAuth = true;
    $mail->Username = $email;
    $mail->Password = $pass;
    // Quien envía este mensaje
    $mail->setFrom($email, $usuario);
    $mails = "marisol.hernandez@ecomtrading.com";
    $mail->addAddress($mails);
    /* $mails = "marisol.hernandez@ecomtrading.com,laura.ruiz@ecomtrading.com,mcisneros@ecomtrading.com";
    $arraycorreo = explode(",",$mails);
    $tam = sizeof($arraycorreo);
    for($i=0; $i<$tam; $i++){
      $mail->addAddress($arraycorreo[$i]);
    }  */

    $mail->Subject = $subject;
    // Contenido
    $mail->IsHTML(true);
    $mail->CharSet = 'UTF-8';  
  //<div style="border: 2px solid #fff; padding: 5px;  max-width: 900px; margin: 0 auto;">
    $bodyy ='
    <div style="">
        <b style="color: #17562c; font-size: 18px;">¡Archivos de Factura recibidos!</b><br><br>
        <b style="color: #17562c; font-size: 15px;">Proveedor: </b> '.$prov.'<br>
        <b style="color: #17562c; font-size: 15px;">Contrato: </b> '.$contrato.'<br>
        <b style="color: #17562c; font-size: 15px;">Liq Id: </b> '.$LiqID.'<br>
        <b style="color: #17562c; font-size: 15px;">Pacas: </b> '.$pacas.'<br><br>
    </div>';
$mail->Body = $bodyy;

// crear el archivo de excel
$filename = 'Fac-'.$LiqID.'.pdf';
$rem='./temp/'.$filename;
$mail->addAttachment($rem); 
$mail->addAttachment($filepath);

$filename1 = 'Fac-'.$LiqID.'.xml';
$rem1='./temp/'.$filename1;
$mail->addAttachment($rem1); 
$mail->addAttachment($filepath1);

if ($mail->send()) {
  echo 'Correo enviado';
} else {
  echo 'Error al enviar el correo: ' . $mail->ErrorInfo;
}

//Borrado de archivos de carpeta temp
function eliminarArch   ($path, $LiqID) {
  $file = $path . '/Fac-'.$LiqID.'.pdf';
  $file1 = $path . '/Fac-'.$LiqID.'.xml';
  if (is_file($file)) {
      unlink($file);
      unlink($file1);
  } 
}

// Llamada a la función para eliminar el archivo específico
eliminarArch    ('./temp', $LiqID);
?>
<!-- <b style="color: black; font-size: 14px;">'.$usu.' a subido sus archivos de factura al portal.<br><br></b> -->