<?php
setlocale(LC_TIME, "spanish");

function lowercase($element){
  return "'".$element."'";
}

$DO = $_GET['DOGIN'];
$Cert = $_GET['Cert'];
//$LotsCMS = $_GET['LotsCMS'];

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
include_once '../fpdf/fpdf.php';

//OBTENER LOS LOTES DE LA DO 

$consulta = "SELECT distinct Lots.Lot from Lots where Lots.DOrd = '$DO';"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();
//obtener solo el array con lotes
$dataDO=$resultado->fetchAll(PDO::FETCH_COLUMN, 0);
//print_r($dataDO);
$LotsCMS=implode(",", $dataDO);


class PDF extends FPDF{
  //Cabecera de pagina
  function Header(){
    $this->Ln(7);
    $this->SetFont('Arial','B',10);
    $this->Image('../img/logo1.png', 14, 13, 13, 13, 'PNG');
    $this->Cell(0,6, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
    $this->Ln(4);
    $this->SetFont('Arial','B',9);
    $this->Cell(0,7,'COTTON DIVISION', 0, 0,'C');
    $this->Ln();
  }

  //Pie de pagina
  function Footer(){
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
  }
}

//OBTENER LAS PACAS DE LOS LOTES QUE ESTAN EN LA DO SELECCIONADA
$consulta = "SELECT distinct Lots.Lot,
        (CASE 
            WHEN Region.IsWHOrigin = 1 
           then (select Region.RegNam)
           ELSE (select Gines.GinName )
        END)  as GinName
        FROM amsadb1.Lots        
        LEFT JOIN amsadb1.DOrds 
        ON Lots.Dord = DOrds.DOrd
        LEFT JOIN amsadb1.Region 
        ON DOrds.OutPlc = Region.IDReg
        LEFT JOIN amsadb1.Gines
        ON DOrds.Gin = Gines.IDGin
        WHERE Lots.Lot in (".$LotsCMS.") AND Lots.DOrd = '$DO'  Order by Lot;"; 
//print_r($consulta);
$resultado = $conexion->prepare($consulta);
$resultado->execute();
$dataLotsCMS=$resultado->fetchALL(PDO::FETCH_ASSOC);

$pdf = new PDF();

foreach ($dataLotsCMS as $dato){
  $pdf->AliasNbPages();
  $pdf->AddPage('Portrait','Letter'); //Landscape
  $pdf->SetTitle("Listado: ".$LotsCMS);
  
  $consulta = 'SELECT Bal, Lot, DO FROM Bales WHERE Lot = '.$dato['Lot'].' Order by Bal;'; 
  $resultado = $conexion->prepare($consulta);
  $resultado->execute();
  $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
  
  $pdf->SetFont('Arial','B', 11);
  $pdf->Cell(1,6, utf8_decode($dato['Lot']),'TB',0,'L');
  $pdf->Cell(0,6, utf8_decode($dato['GinName']),'TB',0,'C');
  if ($Cert == "SI"){
    $pdf->Cell(0,6, 'CERTIFICADAS', 0, 0,'R');
  }
  //$pdf->Cell(0,4, utf8_decode(' ','TB',0,'R');
  $pdf->Ln();
  $i=0;
  foreach ($data as $row) { //foreach($data as $row){
    $i=$i+1;
    if ($i <= 42){
      $pdf->Ln(5);
      $pdf->SetFont('Arial','', 12);
      $pdf->Cell(26,3,$i.".",0,0,'R');
      $pdf->Cell(26,3,($row['Bal']),0,0,'L');
      $x = 39;
    }else if ($i > 42 && $i <= 84){
      //$pdf->Ln(5);
      $pdf->SetXY(73,$x);
      $pdf->SetFont('Arial','', 12);
      $pdf->Cell(26,3,$i.".",0,0,'R');
      $pdf->Cell(26,3,($row['Bal']),0,0,'L');
      $x=$x+5;
      $z = 39;
    }else if ($i > 84){
      //$pdf->Ln(5);
      $pdf->SetXY(137,$z);
      $pdf->SetFont('Arial','', 12);
      $pdf->Cell(26,3,$i.".",0,0,'R');
      $pdf->Cell(26,3,($row['Bal']),0,0,'L');
      $z=$z+5;
    }
  }
}

$pdf->Output('I', "Listado.pdf"); //"Listado_".$GinName."_".date('d-m-Y').".pdf"
$conexion=null;

?>