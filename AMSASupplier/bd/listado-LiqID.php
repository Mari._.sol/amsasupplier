<?php

require './vendor/autoload.php';
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$GinID = $_GET['GinID'];
$gines = explode(",", $GinID);
$gines2 = implode(",", $gines);
$crop = $_GET['crop'];
$opcion = $_GET['opcion'];
$inputValue = $_GET['inputValue'];

$consulta1 = "SELECT GinID, Bal, Lot, DO, LiqID, Grp, Grw, 
                (SELECT GinName FROM Gines WHERE Bales.GinID = Gines.IDGin) as Gin
                FROM Bales
                WHERE BuyIt = 1 AND Crp = $crop AND GinID IN ($gines2)";

$params = [];

if ($opcion == 0) {
    $consulta1 .= " AND Grp = 20";
} elseif ($opcion == 1) {
    $consulta1 .= " AND Lot = ?";
    $params[] = $inputValue;
} elseif ($opcion == 2) {
    $consulta1 .= " AND LiqID = ?";
    $params[] = $inputValue;
} elseif ($opcion == 3) {
    $consulta1 .= " AND (LiqID = 'PENDIENTE' OR LiqID IS NULL OR LiqID = '')";
}

$resultado = $conexion->prepare($consulta1);
$resultado->execute($params);

$fileName = '';
if ($opcion == 0) {
    $fileName = "Listado_de_Pacas_Certificadas.xlsx";
} elseif ($opcion == 1 || $opcion == 2) {
    $fileName = "Listado_de_Pacas_$inputValue.xlsx";
} elseif ($opcion == 3) {
    $fileName = "Listado_de_Pacas_sin_liquidar.xlsx";
}

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Pacas");

$hojaActiva->getColumnDimension('A')->setWidth(17);
$hojaActiva->setCellValue('A1','Gin');
$hojaActiva->getColumnDimension('B')->setWidth(15);
$hojaActiva->setCellValue('B1','No. de paca');
$hojaActiva->getColumnDimension('C')->setWidth(15);
$hojaActiva->setCellValue('C1','Lote');
$hojaActiva->getColumnDimension('D')->setWidth(15);
$hojaActiva->setCellValue('D1','DO');
$hojaActiva->getColumnDimension('E')->setWidth(15);
$hojaActiva->setCellValue('E1','LiqID');
$hojaActiva->getColumnDimension('F')->setWidth(15);
$hojaActiva->setCellValue('F1','Certificadas');
$hojaActiva->getColumnDimension('G')->setWidth(30);
$hojaActiva->setCellValue('G1','Productor');

$fila = 2;

while($row = $resultado->fetch(PDO::FETCH_ASSOC)){
    $Grp = ($row['Grp'] == "20") ? "SI" : "NO";

    $hojaActiva->setCellValue('A' . $fila,$row['Gin']);
    $hojaActiva->setCellValue('B' . $fila,$row['Bal']);
    $hojaActiva->setCellValue('C' . $fila,$row['Lot']);
    $hojaActiva->setCellValue('D' . $fila,$row['DO']);
    $hojaActiva->setCellValue('E' . $fila,$row['LiqID']);
    $hojaActiva->setCellValue('F' . $fila, $Grp);
    $hojaActiva->setCellValue('G' . $fila,$row['Grw']);

    $fila++;
}

$writer = new Xlsx($excel);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
$writer->save('php://output');

exit();
?>
