
<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$GinID = (isset($_POST['GinID'])) ? $_POST['GinID'] : '';
$DO = (isset($_POST['DOrd'])) ? $_POST['DOrd'] : '';
$Lot = (isset($_POST['Lot'])) ? $_POST['Lot'] : '';
$dor = (isset($_POST['dor'])) ? $_POST['dor'] : '';

//$opcion = '4';
//$GinID = "65315,65311";

$myarray = [];
$aux =[];
//$data =array();
$data2 =[];
$myarray = explode(",",$GinID);
$array = implode(",",$myarray);
$tam= sizeof($myarray);

switch($opcion){
    case 1:
        $consulta = "SELECT TrkID,
        (SELECT Typ FROM DOrds WHERE Dord = '$DO') as Tipo,
        (SELECT InReg FROM DOrds WHERE Dord = '$DO') as Region,
        (SELECT Cert FROM DOrds WHERE Dord = '$DO') as Cert,
        (SELECT IF(Tipo='CON',(SELECT InReg FROM DOrds WHERE DOrd = '$DO'),(SELECT InPlc FROM DOrds WHERE DOrd = '$DO') )) as Idcliente,
        (SELECT IF(Tipo='CON',(SELECT RegNam from Region WHERE IDReg = Idcliente),(SELECT Cli FROM Clients WHERE CliId = Idcliente ))) as NomCliente,
        (SELECT RegNam FROM Region WHERE IDReg = Region) as NomRegion
        from Lots WHERE  DOrd='$DO'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;      
    case 2:       
        $consulta = "SELECT Date_Mail From DOrds where DOrd = '$dor'";  
        $resultado = $conexion->prepare($consulta);
        $resultado->execute(); 
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 3:
        $hijo = []; // iniciar array
        $arreglo1 = [];

        $region = 'SELECT IDReg FROM Gines WHERE  IDGin IN ('.$array.')'; 
        $region = $conexion->prepare($region);  
        $region->execute();
        $region = $region->fetch();
	    $region = $region['IDReg'];

        $data2=array();
        $data = [];
        for ($i=0; $i<$tam; $i++){ 
            $consulta = "SELECT DOrd,(SELECT GinName FROM Gines WHERE '$myarray[$i]' = IDGin) as GinName
                FROM DOrds WHERE Gin='$myarray[$i]' and OutPlc ='$region'  ";   
            $resultado = $conexion->prepare($consulta);        
            $resultado->execute(); 

            foreach($resultado as $row){
                $deliveri = $row['DOrd'];                
        
                $query="Select Status from Truks where DO ='$deliveri' AND Status='Programmed' LIMIT 1";
                $resultado2 = $conexion->prepare($query);
                $resultado2->execute(); 
                $datastatus=$resultado2->fetch();                          

                if(!empty($datastatus)){
                    $Qty = $row['Qty'];
                    $GinName =  $row['GinName'];
                    $hijo = [
                        'DOrd' => $deliveri,
                        'GinName' => $GinName,
                    ];
                    array_push($arreglo1, $hijo);          
                }
            }
        } 
        array_push($data2,$arreglo1);

        $longitud = count($data2);
            
        for ($j=0; $j<$longitud;$j++){
            foreach($data2[$j] as $a){
                array_push($data, $a);
            }
        }           
    break;
    case 4:                          
        $consulta = "SELECT distinct DOrds.Cert, DOrds.OutPlc, DOrds.Year,DOrds.DOrd,DOrds.Qty,(DOrds.Tran + DOrds.Rcv) as balestransit, (DOrds.Qty - (DOrds.Tran + DOrds.Rcv)) as balespendientes,DOrds.Date_Mail as Date,
        (CASE 
        WHEN Region.IsWHOrigin = 1 
           then (select Region.RegNam )
           ELSE (select Gines.GinName)
        END)  as GinName
        
        FROM amsadb1.Truks
        LEFT JOIN amsadb1.DOrds
        ON DOrds.DOrd = Truks.DO
        LEFT JOIN amsadb1.Gines
        ON DOrds.Gin = Gines.IDGin
        LEFT JOIN amsadb1.Region 
        ON DOrds.OutPlc = Region.IDReg
        
        WHERE   ((DOrds.Gin IN (".$GinID.") and Region.IsOrigin=1) OR (DOrds.OutPlc IN (".$GinID.") AND Region.IsWHOrigin = 1 )) AND Truks.Status IN ('Programmed','Transit') AND DOrds.Qty > 0 and Truks.CrgQty >0 and (Region.IsWHOrigin = 1 or Region.IsOrigin=1) "; //and DOrds.Date_Mail is not NULL
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC); 
    break;
    case 5:    
        $consulta = "SELECT LotsAssc as Lot,TrkID as Truck,Status,
        (SELECT IFNULL((SELECT TrkLPlt FROM Truks WHERE Truck = TrkID), '') ) as TrkLPlt, 
        (SELECT IFNULL((SELECT RemisionPDF FROM Truks WHERE Truck = TrkID), '') ) as RemisionPDF, 
        (SELECT IFNULL((SELECT DrvNam FROM Truks WHERE Truck = TrkID),'')) as DrvNam,
        (SELECT IFNULL((SELECT Datemailsent FROM Truks WHERE Truck = TrkID),'')) as Datemailsent,
        (SELECT IFNULL((SELECT OutDat FROM Truks WHERE Truck = TrkID),'')) as OutDat,
        (SELECT IFNULL((SELECT OutTime FROM Truks WHERE Truck = TrkID),'')) as OutTime,
        (SELECT IFNULL((SELECT OutDat FROM Truks WHERE Truck = TrkID), '')) as SchOutDate,  
        (SELECT IFNULL((SELECT OutTime FROM Truks WHERE Truck = TrkID), '')) as SchOutTime,
        (SELECT IFNULL((SELECT TraLPlt FROM Truks WHERE Truck = TrkID), '')) as TraLPlt,
        (SELECT IFNULL((SELECT DrvNam FROM Truks WHERE Truck = TrkID), '')) as DrvNam,
        (SELECT IFNULL((SELECT TNam FROM Truks WHERE Truck = TrkID), '')) as nomtransport,
        (SELECT IFNULL((SELECT SUM(Qty) FROM Lots WHERE  DOrd='$DO' AND Truck = TrkID), '')) as Qty,     
        (SELECT IFNULL((SELECT WBill FROM Truks WHERE Truck = TrkID), '')) as 	WBill,
        (SELECT IFNULL((SELECT BnName FROM Transports WHERE nomtransport = TptID),'')) as TptCo
        FROM Truks WHERE DO='$DO' AND Status IN ('Programmed','Transit')";

        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
}
print json_encode($data, JSON_UNESCAPED_UNICODE);

$conexion=null;
//$conexion=null;
