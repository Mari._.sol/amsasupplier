<?php

setlocale(LC_TIME, "spanish");

$TrkID = $_GET['TrkID'];
$TrkDO = $_GET['TrkDO'];
$TrkTyp = $_GET['TrkTyp'];
$Reg = $_GET['Reg'];
$Cli = $_GET['Cli'];




include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

if ($TrkTyp == "CON"){
    $consulta = "SELECT * FROM DOrds WHERE DOrd = '$TrkDO'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataDO=$resultado->fetch();
    $RegIn = $dataDO['InReg'];
    $RegOut = $dataDO['OutPlc'];
    
    $consulta = "SELECT * FROM Region WHERE IDReg = '$RegIn'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataRIn=$resultado->fetch();
    $CdeIn = $dataRIn['Cde'];
    
    $consulta = "SELECT * FROM Region WHERE IDReg = '$RegOut'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataROut=$resultado->fetch();
    $CdeOut = $dataROut['Cde'];
    
}else{
    $consulta = "SELECT * FROM DOrds WHERE DOrd = '$TrkDO'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataDO=$resultado->fetch();
    $InPlc = $dataDO['InPlc'];
    $RegOut = $dataDO['OutPlc'];
    
    $consulta = "SELECT * FROM Clients WHERE CliID = '$InPlc'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataRIn=$resultado->fetch();
    $CdeIn = $dataRIn['Cde'];
    
    $consulta = "SELECT * FROM Region WHERE IDReg = '$RegOut'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();        
    $dataROut=$resultado->fetch();
    $CdeOut = $dataROut['Cde'];
    
}


$consulta = "SELECT * FROM Truks WHERE TrkID = '$TrkID'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$data=$resultado->fetch();

$TName = $data['TNam'];

$consulta = "SELECT Lot, Qty FROM Lots WHERE TrkID = '$TrkID'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();
$dataLots=$resultado->fetchAll(PDO::FETCH_ASSOC);


$consulta = "SELECT BnName FROM Transports WHERE TptID = '$TName'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();        
$dataTpt=$resultado->fetch();

if ($Reg != "CLIENTE"){
    $consulta = "SELECT * FROM Region WHERE RegNam = '$Reg'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataCli=$resultado->fetch();
    $RemTo = $dataCli['BnName'];
    $Drc = $dataCli['Drctn'];
    $CP = $dataCli['CP'];
    $Town = $dataCli['Town'];
    $State = $dataCli['State'];
    $Ct1 = $dataCli['Ct1'];
    $Tel1 = $dataCli['Tel1'];
    $Ct2 = $dataCli['Ct2'];
    $Tel2 = $dataCli['Tel2'];
    
    $result1 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($Tel1)), 2); //Dar formato tel xxx-xxx-xxxx
    $result2 = preg_replace('/\d{3}/', '$0-', str_replace('.', null, trim($Tel2)), 2); //Dar formato tel xxx-xxx-xxxx
    
}else{
    $consulta = "SELECT * FROM Clients WHERE Cli = '$Cli'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $dataCli=$resultado->fetch();
    $RemTo = $dataCli['BnName'];
    $Drc = $dataCli['Drctn'];
    $CP = $dataCli['CP'];
    $Town = $dataCli['Town'];
    $State = $dataCli['State'];
}


include_once '../fpdf/fpdf.php';

$pdf = new FPDF();
$pdf->AddPage('portrait');
$pdf->SetTitle($TrkID);
$pdf->SetFont('Arial','B',10);
$pdf->Image('../img/logo1.png', 15, 10, 20, 20, 'PNG');
$pdf->Cell(0,10, utf8_decode('AGROINDUSTRIAS UNIDAS DE MEXICO S.A. DE C.V.'),0,0,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0,10,'COTTON DIVISION', 0, 0,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial','',8);
$pdf->Cell(0,10,'Calzada de los Forjadores No. 1250, Brittingham, Gomez Palacio, Durango, C.P. 35030, Tel. (871)7145304', 0, 0,'C');

$pdf->Ln(8);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0,10,'CARTA REMISION', 0, 0,'C');

//CUERPO REMISION
$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(150,8,'Numero: ', 0, 0,'R');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, $TrkID, 0, 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(150,8,'Fecha de carga: ', 0, 0,'R');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, strtoupper(strftime('%d/%b/%Y', strtotime($data['OutDat']))), 0, 0, 'L'); //fecha español dd/mm/yyyy
//$pdf->Cell(0, 8, strtoupper(date('d/M/Y', strtotime($data['OutDat']))), 0, 0, 'L'); //fecha ingles dd/mm/yyyy
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(150,8,'Fecha de entrega: ', 0, 0,'R');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, strtoupper(strftime('%d/%b/%Y', strtotime($data['InDat']))), 0, 0, 'L'); //fecha español dd/mm/yyyy
//$pdf->Cell(0, 8, strtoupper(date('d/M/Y', strtotime($data['InDat']))), 0, 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(150,8,'Hora de entrega: ', 0, 0,'R');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, substr($data['InTime'], -8, 5), 0, 0, 'L');

$pdf->Ln(9);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,8,'Remitido a: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, utf8_decode($RemTo), 0, 0, 'L'); //buscar en tabla Reg o Client dependiendo de CON, DOM, Exp
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,8,'Direccion: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, strtoupper(utf8_decode($Drc)).", C.P. ".$CP, 0, 0, 'L'); //buscar en tabla Reg o Client dependiendo de CON, DOM, Exp
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,8,'Destino: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, strtoupper($Town).", ".strtoupper($State), 0, 0, 'L'); //buscar en tabla Reg o Client dependiendo de CON, DOM, Exp

$pdf->Ln(9);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,8,'Descripcion: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(37, 8, utf8_decode("PACAS DE ALGODÓN"), 0, 0, 'L');
$pdf->Ln(7);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,5,'Marcas: ', 0, 0,'L');
foreach($dataLots as $row){
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(13, 5, $row['Lot'], 'R', 0, 'C');
}
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,5,'Cantidad: ', 0, 0,'L');
foreach($dataLots as $row){
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(13, 5, $row['Qty'], 'R', 0, 'C');
}
/*$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,8,'Cantidad: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, $dataLots['CrgQty'], 0, 0, 'C');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,8,'Marcas: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, $data['LotsAssc'], 0, 0, 'L');*/

$pdf->Ln(6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,8,'Carta porte: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(37, 8, $data['WBill'], 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,8,'Tracto: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(23, 8, $data['TrkLPlt'], 0, 0, 'C');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,8,'Placas: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, $data['TraLPlt'], 0, 0, 'L');

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(25,8,'Obsrvaciones: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, $data['Comments'], 0, 0, 'L');

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Linea de Transporte: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, utf8_decode($dataTpt['BnName']), 0, 0, 'L');

$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Nombre de chofer: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(80, 8, utf8_decode($data['DrvNam']), 0, 0, 'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(30,8,'Orden Embarque: ', 0, 0,'L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(0, 8, $TrkDO, 0, 0, 'L');

$pdf->Image('../img/remision.png', 15, 128, 35, 55, 'PNG');

$pdf->Ln(24);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','',6);
$pdf->MultiCell(80, 4, utf8_decode('ESTOY DE ACUERDO CON LAS CONDICIONES DE VIAJE Y SEGUIR CON LOS PROTOCOLOS DE SEGURIDAD DE AMSA'),  0, 'L', false);
$pdf->Ln(8);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0, 8, "PROTOCOLO DE VIAJE NO CONTRATAR GUIA", 'T', 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','',5);
$pdf->Cell(0, 8, "*EN VIAJES AL CENTRO DEL PAIS, PUEBLA, EDO MEXICO, HIDALGO, UNICO PARADERO AUTORIZADO PARA PASAR LAS NOCHES ES EL PARADERO SAN PEDRO", 0, 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','',5);
$pdf->Cell(0, 8, "*EN CASO DE FALLA DE LA UNIDAD, ENFERMEDAD DEL OPERADOR CUALQUIER OTRO IMPONDERABLE, COMUNICARSE INMEDIATAMENTE", 0, 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','',5);
$pdf->Cell(0, 8, utf8_decode("*SE PROHIBE LA ENTRADA AL ARCO NORTE DE NOCHE POR SEGURIDAD, DEPENDIENDO LA EPOCA DEL AÑO ANTES DE LAS 19:00 HRS"), 0, 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','',5);
$pdf->Cell(0, 8, "*CONTINUE SU RUTA A PARTIR DE LAS 6:00 AM HACIA SU DESTINO", 0, 0, 'L');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(45, 8,'', 0, 0,'L');
$pdf->SetFont('Arial','',5);
$pdf->Cell(0, 8, "*SI NO ENCUENTRA LA DIRECCION DE ENTREGA, INMEDIATAMENTE COMUNICARSE CON LOS SIGUIENTES NUMEROS DE 6:00 AM A 10:00 PM, NO BUSCAR GUIAS", 0, 0, 'L');

$pdf->Ln(10);
$pdf->SetFont('Arial','B',7.3);
$pdf->Cell(0, 8, "SR. OPERADOR: LAS EVIDENCIAS DE ENTREGA (CARTA REMISION, LISTADO DE PACAS, CARTA PORTE) TODAS DEBEN SER SELLADAS Y/O FIRMADAS", 0, 0,'L');
$pdf->Ln(9);
$pdf->SetFont('Arial','B',7.3);
$pdf->MultiCell(0, 4, "ESTIMADO OPERADOR ES SU OBLIGACION ESTAR PENDIENTE DE LA CARGA, CUALQUIER FALTANTE AL MOMENTO DE LA DESCARGA SE REALIZARA EL CARGO CORRESPONDIENTE.", 0, 'C', false);

$pdf->Ln(5);
if ($Reg != "CLIENTE"){
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(50, 8,'VIAJE A AMSA '.$Reg, 'TL', 0,'C');
    $pdf->SetFont('Arial','B',6);
    $pdf->Cell(50, 8, "CONTACTO 1: ".$Ct1, 'TR', 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(50, 8,'', 'L', 0,'R');
    $pdf->SetFont('Arial','B',6);
    $pdf->Cell(50, 8, "TELEFONO: ".$result1, 'R', 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(50, 8,'', 'L', 0,'R');
    $pdf->SetFont('Arial','',6);
    $pdf->Cell(50, 8, "CONTACTO 2: ".$Ct2, 'R', 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(50, 8,'', 'LB', 0,'R');
    $pdf->SetFont('Arial','',6);
    $pdf->Cell(50, 8, "TELEFONO: ".$result2, 'RB', 0, 'L');
}else{
    if ($State == 'PUEBLA'){
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(50, 8,'VIAJE A AMSA PUEBLA', 'TL', 0,'C');
    /*$pdf->SetFont('Arial','B',6);
    $pdf->Cell(50, 8, "CONTACTO 1: Christian Becerra", 'TR', 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(50, 8,'', 'L', 0,'R');
    $pdf->SetFont('Arial','B',6);
    $pdf->Cell(50, 8, "TELEFONO: 222-650-0994", 'R', 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(50, 8,'', 'L', 0,'R');*/
    $pdf->SetFont('Arial','',6);
    $pdf->Cell(50, 8, "CONTACTO 2: Paulo Rojas", 'TR', 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(50, 8,'', 'LB', 0,'R');
    $pdf->SetFont('Arial','',6);
    $pdf->Cell(50, 8, "TELEFONO: 222-650-0940", 'RB', 0, 'L');
    }else{
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(50, 8,'EMBARQUE A CLIENTE', 'TL', 0,'C');
    $pdf->SetFont('Arial','B',6);
    $pdf->Cell(50, 8, "CONTACTO 1: BRUNO BARRIENTOS", 'TR', 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(50, 8,'', 'LB', 0,'R');
    $pdf->SetFont('Arial','B',6);
    $pdf->Cell(50, 8, "TELEFONO: 618-134-0710", 'RB', 0, 'L');
    $pdf->Ln(5);
    }
    /*$pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(50, 8,'', 'L', 0,'R');
    $pdf->SetFont('Arial','',6);
    $pdf->Cell(50, 8, "CONTACTO 2: HUGO PAYAN", 'R', 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(50, 8,'', 0, 0,'R');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(50, 8,'', 'LB', 0,'R');
    $pdf->SetFont('Arial','',6);
    $pdf->Cell(50, 8, "TELEFONO: 868-818-2672", 'RB', 0, 'L');*/
}


$pdf->Ln(12);
$pdf->SetFont('Arial','B',7.3);
$pdf->MultiCell(0, 4, "****EN CASO DE NO SEGUIR CON LOS PROTOCOLOS DE SEGURIDAD AL PIE DE LA LETRA DE TRANSPORTE SE DARA DE BAJA COMO PROVEEDOR DE SERVICIO DE MANERA INMEDIATA Y EN CASO DE ROBO SE DETENDRAN LOS PAGOS.", 0, 'C', false);


$pdf->Ln(8);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(0, 8,'REMITE', 0, 0,'C');
$pdf->Ln(17);
$pdf->SetFont('Arial','',8);
$pdf->Cell(65, 8, "", 0, 0, 'C');
$pdf->Cell(60, 8, "AGROINDUSTRIAS UNIDAS DE MEXICO SA DE CV", 'T', 0, 'C');
$pdf->Cell(65, 8, "", 0, 0, 'C');

$pdf->Output('I', $TrkDO.".pdf");

$conexion=null;

?>
