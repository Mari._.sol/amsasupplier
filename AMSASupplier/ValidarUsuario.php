<?php 

session_start();
date_default_timezone_set("America/Mexico_City");
$usu=$_POST['username'];
$pas=$_POST['password'];
$fechaActual = date('d-m-Y');
$fechaActual = new DateTime($fechaActual);
$fechalogin = date('d-m-Y h:i:s a', time());  
//$paises = array("MX", "US","CA");
//define("CLAVE_SECRETA", "6Lelv90iAAAAAJKYJXaPWje216UxfZQtQf4kF3iw");

//clave pruebas
define("CLAVE_SECRETA", "6Ld5tsoiAAAAAK5OYomTjMajENEgRmby3QRJH52E");
$ip="0";

# Comprobamos si enviaron el dato
if (!isset($_POST["g-recaptcha-response"]) || empty($_POST["g-recaptcha-response"])) {
	include_once("errorUser.php");
}

$DateAndTime = date('d-m-Y h:i:s a', time());  
$token = $_POST["g-recaptcha-response"];
$verificado = verificarToken($token, CLAVE_SECRETA);

//---------------------------------------SI SE ENVIO EL CAPCHA CORRECTAMENTE-------------------------------------------------------
if ($verificado) {
    //$link=mysqli_connect("database-1.cayybhjfqryx.us-east-2.rds.amazonaws.com","admin","Amsa.Cotton2023!"); //,"admin","admin2021");
    //$link=mysqli_connect("localhost","root",""); //Puebas en servidor local
    //mysqli_select_db($link,"database_amsa");
    $link=mysqli_connect("localhost:3307","root",""); //Puebas en servidor local
    mysqli_select_db($link,"amsadb1");


    //$result=mysqli_query($link,"select UserID,Password,Tipo,Nombre from Usuarios where UserID='$usu'");
    $result=mysqli_query($link,"SELECT * FROM proveed WHERE ProveedID='$usu'");

    if($row=mysqli_fetch_array($result)){
        // Comparar la ultima fecha de actualizacion de contraseña con la fecha de hoy 
        $Date = date("d-m-Y", strtotime($row['Passupdate']));
        $Date = new DateTime($Date);
        $interval = date_diff($Date, $fechaActual);
        $interval->format('%a'); // Retorna la diferencia de dias 
        $comp = strcmp($row['Password'],"Inicio01"); //Valida si es un password provisional

        //echo "Si existe el usuario <br>";
        if($row['Password']==$pas &&  $row['Locked'] == 0 ){   
            //Primera condicion valida cada cuantos días debe actualizar el password
            //La Segunda condicion valida que no sea un password provicional 
			if (($interval->format('%a')) < 90 &&  $comp != 0 ){  
                $_SESSION['proveed']=$row['ProveedID'];
                $_SESSION['pass']=$row['Password'];
                $_SESSION['gin']=$row['GinID'];
                $_SESSION['LoadCertificados']=$row['LoadCertificados'];
                $_SESSION['ViewLiq']=$row['ViewLiq'];
                //echo"es correcto el password <br>";
                //include_once('doproveed.php');
                $ip = getRealIP();
                $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
                $pais=$ipdat->geoplugin_countryCode; 
            
                $query=mysqli_query($link,"UPDATE proveed set Attempts='0',Last_Login = '$fechalogin' where ProveedID='$usu'");
                $query=mysqli_query($link,"INSERT INTO Registros (Usuario,IP,Date) VALUES ('$usu','$ip','$DateAndTime')");
          
                if (in_array($pais, $paises)) {
                    //header("Location:doproveed.php");  
                    if($row['Version_V2'] == 0){
                        header("Location:doproveed.php");
                    }else{
                        header("Location:doproveed_v2.php");
                    }                         
                }else{
                    header("Location:ipinvalida.php");  
                }
			}else{// condicion en caso de que se requiera actualizar la contraseña.
				$_SESSION['proveed']=$row['ProveedID'];
				$_SESSION['pass']=$row['Password'];
				$_SESSION['gin']=$row['GinID'];
                $_SESSION['LoadCertificados']=$row['LoadCertificados'];
                if($row['Version_V2'] == 0){
                    include_once('passwordupdate.php');
                }else{
                    include_once('passwordupdate_v2.php');
                }
                }
            //if($row['Lots']==0) header("Location:trucks.php");
            //if($row['Lots']==1) header("Location:lots.php");

            /*if($row['Tipo']==0) header("Location:trucks.php");
            if($row['Tipo']==1) header("Location:lots.php");
            if($row['Tipo']==2) header("Location:lots2.php");*/
        }else{
            if($row['Password']!=$pas &&  $row['Attempts'] < 2 && $row['Locked'] == 0){	
                include_once("errorIndex.php");
                $intento= $row['Attempts'] + 1;
                $query=mysqli_query($link,"UPDATE proveed set Attempts='$intento' where ProveedID='$usu'");
            } 

            if($row['Attempts'] == 2 && $row['Locked'] == 0){	
                $query=mysqli_query($link,"UPDATE proveed set Locked='1' where ProveedID='$usu'");
                $query=mysqli_query($link,"UPDATE proveed set Attempts='0' where ProveedID='$usu'");
                include_once("userblocked.php");
            }

            if($row['Locked'] == 1){	
                include_once("userblocked.php");
            }
        }
    }else{
        include_once("errorUser.php");
    }
} 
//---------------------------------------SI SE ENVIO EL CAPCHAT NO SE ENVIA ----------------------------------------------------
else {
	include_once("errorUser.php");
}

//---------------------------------------Funcion para validar token de capchat----------------------------------------------------
function verificarToken($token, $claveSecreta){
    # La API en donde verificamos el token
    $url = "https://www.google.com/recaptcha/api/siteverify";
    # Los datos que enviamos a Google
    $datos = [
        "secret" => $claveSecreta,
        "response" => $token,
    ];
    // Crear opciones de la petición HTTP
    $opciones = array(
        "http" => array(
            "header" => "Content-type: application/x-www-form-urlencoded\r\n",
            "method" => "POST",
            "content" => http_build_query($datos), # Agregar el contenido definido antes
        ),
    );
    // # Preparar petición
    $contexto = stream_context_create($opciones);
    //# Hacerla
    $resultado = file_get_contents($url, false, $contexto);
    //# Si hay problemas con la petición (por ejemplo, que no hay internet o algo así)
    //# entonces se regresa false. Este NO es un problema con el captcha, sino con la conexión
    //# al servidor de Google
    if ($resultado === false) {
        //  # Error haciendo petición
        return false;
    }

    //# En caso de que no haya regresado false, decodificamos con JSON
    //# https://parzibyte.me/blog/2018/12/26/codificar-decodificar-json-php/

    $resultado = json_decode($resultado);
    //# La variable que nos interesa para saber si el usuario pasó o no la prueba
    //# está en success
    $pruebaPasada = $resultado->success;
    //# Regresamos ese valor, y listo (sí, ya sé que se podría regresar $resultado->success)
    return $pruebaPasada;
}

//else 	header("Location:errorusuario.php");;
//echo "Usuario= $usu<br>";
//echo "password= $pas<br>";
function getRealIP(){
    if (isset($_SERVER["HTTP_CLIENT_IP"])){
        return $_SERVER["HTTP_CLIENT_IP"];
    }elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    }elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
        return $_SERVER["HTTP_X_FORWARDED"];
    }elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
        return $_SERVER["HTTP_FORWARDED_FOR"];
    }elseif (isset($_SERVER["HTTP_FORWARDED"])){
        return $_SERVER["HTTP_FORWARDED"];
    }else{
        return $_SERVER["REMOTE_ADDR"];
    }
}

function getCountry($ip_address){
	// código por Marc Palau
	$url = "http://ip-to-country.webhosting.info/node/view/36";
	$inici = "src=/flag/?type=2&cc2=";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST,"POST");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "ip_address=$ip_address");
	ob_start();
	curl_exec($ch);
	curl_close($ch);
	$cache = ob_get_contents();
	ob_end_clean();
	$resto = strstr($cache,$inici);
	$pais = substr($resto,strlen($inici),2);
	return $pais;
}

?>

