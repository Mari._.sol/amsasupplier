$(document).ready(function() {
  $('#formulario').submit(function(e) {
      e.preventDefault();

      // Obtener los valores de los campos
      var GinID  = $.trim($('#GinID').val());
      var crop = $('#crop').val();
      var opcion = $('#opcion').val();
      var inputValue = '';
      if (opcion == 1) {
          inputValue = $('#inputLote').val();
      } else if (opcion == 2) {
          inputValue = $('#inputLiqId').val();
      }
      
      // Pasar los valores a través de la URL
      window.open("./bd/listado-LiqID.php?crop=" + encodeURIComponent(crop) + "&opcion=" + encodeURIComponent(opcion) + "&inputValue=" + encodeURIComponent(inputValue) + "&GinID=" + encodeURIComponent(GinID));
      /* var url = "./bd/listado-LiqID.php?crop=" + encodeURIComponent(crop) + "&opcion=" + encodeURIComponent(opcion) + "&inputValue=" + encodeURIComponent(inputValue) + "&GinID=" + encodeURIComponent(GinID);
      window.location.href = url; */

      /* crop = $('#crop').val(2024);
      opcion = $('#opcion').val(0);
      inputValue = $('#inputLote').prop('disabled', true).val('');
      inputValue = $('#inputLiqId').prop('disabled', true).val(''); */
  });

  // Función para manejar las opciones
  function opciones() {
      var opcion = $('#opcion').val();
      var inputLote = $('#inputLote');
      var inputLiqId = $('#inputLiqId');
      
      // Restablecer el valor del input a vacío cuando se deshabilite
      if (opcion == 1) {
          inputLote.prop('disabled', false);
          inputLiqId.prop('disabled', true).val('');
      } else if (opcion == 2) {
          inputLote.prop('disabled', true).val('');
          inputLiqId.prop('disabled', false);
      } else {
          inputLote.prop('disabled', true).val('');
          inputLiqId.prop('disabled', true).val('');
      }
  }    

  // Llamar a la función opciones() cuando se cambie la opción
  $('#opcion').change(opciones);
  opciones();
});