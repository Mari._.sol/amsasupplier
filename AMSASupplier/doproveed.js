var DOrd;
$(document).ready(function() {

    //funcion para sumar
jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
    return this.flatten().reduce( function ( a, b ) {
      if ( typeof a === 'string' ) {
        a = a.replace(/[^\d.-]/g, '') * 1;
      }
      if ( typeof b === 'string' ) {
        b = b.replace(/[^\d.-]/g, '') * 1;
      }
      return a + b;
    }, 0);
});


    
    
    var opcion;
    opcion = 4;
    var GinID  = $.trim($('#GinID').val());
    
    tablaCli = $('#tablaClients').DataTable({

        drawCallback: function () {
            var api = this.api();
    
            var balesmoved = api.column( 4, {"filter":"applied"}).data().sum();
            var totaldo = api.rows({"filter":"applied"}).count();
           // var pesototal = api.column( 11, {"filter":"applied"}).data().sum();
            $('#balesmoved').html(balesmoved);
            $('#totaldo').html(totaldo);
           // $("#totallotes").val(numlotes);
         //   $('#totalpeso').html(pesototal);
    
          },
     
       
        responsive: "true",
       
        
      
        "order": [ 5,1, 'desc' ],
        "scrollX": false,
        "scrollY": "40vh",
        "scrollCollapse": true,
        "lengthMenu":false,
        "bPaginate": false,
        "bLengthChange": false,
        "bInfo": false,
       
        "ajax":{            
            "url": "bd/crudproveddo.php", 
            "method": 'POST', //usamos el metodo POST
            "data":{opcion:opcion, GinID:GinID}, //enviamos opcion 4 para que haga un SELECT
         //   success: function(data) {
          //      console.log(typeof data)
           // },
            "dataSrc":""
        },
        "columns":[

            {"defaultContent":"<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnmore'><i class='material-icons'>search</i></button><button class='btn btn-success  btn-sm btnlistado' title='Listado de pacas' ><i class='material-icons'>list</i></button></div></div>",width: '50px'},
            {"data": "DOrd"},
            {"data": "Qty"},
            {"data": "balestransit"},
            {"data": "balespendientes"},
            {"data": "Year"},
            {"data": "GinName"},          
            {"data": "Date"}
          
        ],
      
    });     

    var oTable = $('#tablaClients').DataTable();
    
    oTable.on("click", ".btnmore", function(){
        //$('#tablatrucks .editable').empty();
       // $('tbody tr td:nth-child(2)').text('');
      // $('#tablatrucks').DataTable().clear()
      /*
      tablatrk= $('#tablatrucks').DataTable();
      tablatrk.fnClearTable();
      tablatrk.fnDraw();
      tablatrk.fnDestroy();
      */

       $('#tablatrucks').DataTable().destroy();
        $('#tablatrucks tbody').empty();
        let tableBody = document.getElementById('tbody');
        var pval="";

       

           // var i = $(this).find('i');
           // i.toggleClass('fa-minus').toggleClass('fa-plus');
         
      //  var i = $(this).find('i');
       // i.attr('class', i.hasClass('fa-plus-circle') ? 'fa fa-minus' : i.attr('data-original'));

        opcion = 5;
        fila = $(this).closest("tr");
        
        DOrd = parseInt(fila.find('td:eq(1)').text());
       // console.log(opcion);
       // console.log(DOrd);
        var datos1 = "";
        
        var transportname;

        
        var boton1 = '<button class="btn btn-success btn-sm btnremision" title="Generar remision"><i class="material-icons">local_shipping</i></button>';
        //var boton2 = '<button class="btn btn-danger btn-sm btnpdfrem" title="Descargar carta porte "><i class="material-icons">picture_as_pdf</i></button>';
        var boton3 = '<button class="btn btn-primary btn-sm btndescargar" title="Descargar carta porte "><i class="material-icons">file_download</i></button>';
 


        let tr = $(this).closest('tr');
        let row = oTable.row(tr);

       /* if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }*/
        //else {
            $.ajax({           
                "url": "bd/crudproveddo.php", 
                "method": 'POST', //usamos el metodo POST
                //"dataType": 'json',
                "data":{opcion:opcion, DOrd:DOrd}, //enviamos opcion 4 para que haga un SELECT
                success: function(data) {
               // console.log("Datos enviados con exito"); 
                datos1 = JSON.parse(data);
                for (var i = 0; i< datos1.length; i++){
                     transportname = datos1[i].TptCo;
                     plateTruck =   datos1[i].TrkLPlt;


                   /* if(transportname===null || plateTruck== null){
                        transportname="";
                        plateTruck = ""

                    }
                    */
              
                    if(datos1[i].Truck!='0' && datos1[i].Datemailsent!='' ){
                        if(datos1[i].RemisionPDF!='0' ){
                            pval += '<tr><td>'+boton1  +'</td><td>'+boton3+'</td><td>'+datos1[i].Lot+'</td><td>'+datos1[i].Qty+'</td><td class="celdaAsignado">'+ datos1[i].SchOutDate+'</td><td>'+ datos1[i].SchOutTime+'</td><td>'+datos1[i].Truck+'</td><td class="celdaAsignado">'+ transportname+'</td><td>' +
                            datos1[i].TrkLPlt+'</td><td>'+ datos1[i].TraLPlt+'</td><td class="celdaAsignado">'+datos1[i].DrvNam+ '</td><td>'+datos1[i].WBill + '</td><td>'+DOrd+ '</td></tr>';
                        }
                        else{
                            pval += '<tr><td>'+boton1  +'</td><td>'+''+'</td><td>'+datos1[i].Lot+'</td><td>'+datos1[i].Qty+'</td><td class="celdaAsignado">'+ datos1[i].SchOutDate+'</td><td>'+ datos1[i].SchOutTime+'</td><td>'+datos1[i].Truck+'</td><td class="celdaAsignado">'+ transportname+'</td><td>' +
                            datos1[i].TrkLPlt+'</td><td>'+ datos1[i].TraLPlt+'</td><td class="celdaAsignado">'+datos1[i].DrvNam+ '</td><td>'+datos1[i].WBill + '</td><td>'+DOrd+ '</td></tr>';
                        }       
                
                    } 
                    else{
                        pval += '<tr><td >'+' ' +'</td><td >'+' ' +'</td><td>'+datos1[i].Lot+'</td><td>'+datos1[i].Qty+'</td><td class="celdaAsignado">'+ datos1[i].SchOutDate+'</td><td>'+ datos1[i].SchOutTime+'</td><td>'+datos1[i].Truck+'</td><td class="celdaAsignado">'+ transportname+'</td><td>' +
                        datos1[i].TrkLPlt+'</td><td>'+ datos1[i].TraLPlt+'</td><td class="celdaAsignado">'+datos1[i].DrvNam+ '</td><td>'+datos1[i].WBill + '</td><td>'+DOrd+ '</td></tr>';

                    }              
                }

                
                tableBody.innerHTML += pval;/*
                // Open this row
                 row.child('<div cass="view">'+ '<div cass="wrapper">'+'<table class="table table-striped contenedor-tabla tabla2"  id="tablalotes" style="padding-left:50px;">'
                            + '<thead>' +
                            '<tr style="color:white;background-color:grey">' +
                            '<th scope="col class ="sticky-col second-col"></th>' +
                            '<th scope="col class ="sticky-col second-col"></th>' +
                            '<th scope="col class ="sticky-col second-col"></th>' +
                            ' <th scope="col">Lot</th>' +
                            '<th scope="col">Quantity</th>' +                            

                            '<th scope="col">Truck Arrival Date</th>' +
                            '<th scope="col">Truck Arrival Time</th>' +
    
                       
                            '<th scope="col">Transport ID</th>' + 
                            '<th scope="col" >Transport</th>' + 
                            '<th scope="col">Truck Plate</th>' +                            
                            '<th scope="col">Trailer Plate</th>' +
                            
                            '<th scope="col">Driver Name</th>' +
                            '<th scope="col">Waybill</th>' +
                            '<th scope="col">DO</th>' +
                            
                            ' </tr>' +
                            '</thead>' + '<tbody class="datos" >' + '<tr>' +  pval + 
                              
           
           
           
                            '</tr>' +'</tbody>'+ '</table>' + '</div>'+ '</div>'//+
                           // '<style type="text/css">'+'table {border-collapse: collapse;width: 100%;}'+
                           // 'table thead tr th {background-color: red;position: sticky;z-index: 100;top: 0;}'+
                              

                           // '</style>'
                            
             
           ).show();
            tr.addClass('shown');

        },
        error: function (data){
            console.log("Error al enviar datos");
        }*/
    
        }
        
        
         });
        






       // tableBody += "";


    });


    $(document).on("click", ".btnremision", function(){
        fila = $(this);           
        Lot = $(this).closest('tr').find('td:eq(2)').text();
        DOrd = $(this).closest('tr').find('td:eq(12)').text();
        idtruck = $(this).closest('tr').find('td:eq(6)').text();
        console.log(DOrd);

      //  console.log('DELIVERI  ' + truckid);
       // TrkDO = $(this).closest('tr').find('td:eq(1)').text();
       // TrkTyp = $(this).closest('tr').find('td:eq(2)').text();
        //Reg = $(this).closest('tr').find('td:eq(15)').text();
        //Cli = $(this).closest('tr').find('td:eq(17)').text();
        opcion = 1;
        $.ajax({           
            "url": "bd/crudproveddo.php", 
            "method": 'POST', //usamos el metodo POST
            //"dataType": 'json',
            "data":{opcion:opcion, DOrd:DOrd, Lot:Lot}, //enviamos opcion 4 para que haga un SELECT
            success: function(data) {
                datos2 = JSON.parse(data);
                //console.log(data);
               // console.log(datos2[0].TrkID);

                window.open("./bd/PDFREM.php?TrkID="+idtruck+"&TrkDO="+DOrd+"&TrkTyp="+datos2[0].Tipo+"&Reg="+datos2[0].NomRegion+"&Cli="+datos2[0].NomCliente+"");
            },
            error: function (data){
                console.log("Error al enviar datos");
            }

        });   

    });

    $(document).on("click", ".btnpdfrem", function(){
        fila = $(this);           
        idtruck = $(this).closest('tr').find('td:eq(6)').text();
        window.open("opens3.php?TrkID="+idtruck);
      //  console.log('DELIVERI  ' + truckid);
       // TrkDO = $(this).closest('tr').find('td:eq(1)').text();
       // TrkTyp = $(this).closest('tr').find('td:eq(2)').text();
        //Reg = $(this).closest('tr').find('td:eq(15)').text();
        //Cli = $(this).closest('tr').find('td:eq(17)').text();
           


       
       
    });

    $(document).on("click", ".btndescargar", function(){
        fila = $(this);           
       // Lot = $(this).closest('tr').find('td:eq(3)').text();
        idtruck = $(this).closest('tr').find('td:eq(6)').text();
        //DOrd = $(this).closest('tr').find('td:eq(13)').text();

        window.open("./bd/descargar-cartaporte.php?idtruck="+idtruck);
      

      //  console.log('DELIVERI  ' + truckid);
       // TrkDO = $(this).closest('tr').find('td:eq(1)').text();
       // TrkTyp = $(this).closest('tr').find('td:eq(2)').text();
        //Reg = $(this).closest('tr').find('td:eq(15)').text();
        //Cli = $(this).closest('tr').find('td:eq(17)').text();
       

    });
    

    $('#plan').on('click', function(e) {
        var GinID  = $.trim($('#GinID').val());
        //var user  = $.trim($('#usuario').val());

        e.preventDefault(); 
        window.open("./bd/plan-embarques_V2.php?user="+GinID);
    

    });

    $('#manual').on('click', function(e) {
        var GinID  = $.trim($('#GinID').val());
        //var user  = $.trim($('#usuario').val());

        e.preventDefault(); 
        window.open("./bd/manual-user.php");
    

    });
     
    

    $(document).on("click", ".btnlistado", function(e){
        fila = $(this);           

        dor = $(this).closest('tr').find('td:eq(1)').text();       
       // alert(dor);
       //Opcion para consultar si ya enviaron correo de listados 
       opcion = 2;	


       $.ajax({           
        "url": "bd/crudproveddo.php", 
        "method": 'POST', //usamos el metodo POST
        //"dataType": 'json',
        "data":{opcion:opcion, dor:dor},
        success: function(data) {
            datos2 = JSON.parse(data);
            
            fecha = datos2[0].Date_Mail;
            console.log(fecha);
            if(fecha =="" || fecha == null){
                alert("Listado aun no disponible")
            }
            else{
                e.preventDefault(); 
                window.open("./bd/listado-pacas.php?dor="+dor);

            }
            
             
        },
        error: function (data){
            console.log("Error al enviar datos");
        }

    });



       

    });

    





         
});
    

    