<?php
 session_start();
if (!isset($_SESSION['proveed'])) :
    include_once('index.php');
else :
    $Gin = $_SESSION['gin'];
    $usuario=$_SESSION['proveed'];
    $ViewLiq=$_SESSION['ViewLiq'];
?>

<!doctype html>
<html lang="es en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="img/ecom.png" />
    <title>Liquidaciones</title>

    <!-- CSS bootstrap -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
    <!--datables estilo bootstrap 4 CSS-->
    <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

    <!-- CSS personalizado -->
    <link rel="stylesheet" href="main.css">
    
    <!--Google fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--font awesome con CDN  -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
</head>

<body>
    <!-- Scripts -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

    <!-- jQuery, Popper.js, Bootstrap JS -->
    <script src="assets/jquery/jquery-3.3.1.min.js"></script>
    <script src="assets/popper/popper.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- librerias necesarias para finalizar sesion por inactividad -->
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <!-- Scrip para finalizar sesion por inactividad -->
    <script type="text/javascript" src="timer.js"></script>

    <!-- datatables JS -->
    <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>
    
    <!-- para usar botones en datatables JS -->
    <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
    <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
    <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
    <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
    
    <!-- Ficed columns -->
    <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>
    <script type="text/javascript" src="mainLiq.js"></script>

    <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
    <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
        <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
            <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                    </svg>
                </a>

                <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                <?php if ($ViewLiq == 1){?>
                    <li><a class="dropdown-item" style="background-color: #5a926d;" href="Liquidations.php">Liquidaciones</a></li>
                <?php } ?>
                    <li><a class="dropdown-item" href="purchases_v2.php">Listados</a></li>
                    <li><a class="dropdown-item" href="doproveed_v2.php">Embarques</a></li>
                </ul>
                <div id="gin" style="display:none;">
                    <input id="GinID" value="<?php echo $Gin; ?>"/>
                    <input id="usuario" value="<?php echo $usuario; ?>"/>
                </div> 
            </div>
            <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="Liquidations.php">
                <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
            </a>
            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                <p>/ Liquidaciones</p>
            </div>
            <!-- Botones -->
            <div class="container-fluid  col-xl-2 col-lg-2 col-md-1 col-sm-1 col-xs-1">
                <button id="manual" class="btn btn-secondary" title="Manual de Usuario"><i class="bi bi-info-circle-fill"></i></button>
            </div>
            <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['proveed']; ?></button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                    <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Inicio de la tabla -->
    <div class="card card-body " style="opacity:100%;" >
        <div class="table-responsive" style="opacity:100%;">
            <table id="tablaLiquidation" class="table bg-white table-striped row-border order-column table-hover" style="opacity:100%;">
                <thead style="background-color: #65ac7c;" style="opacity:100%;">
                    <tr>
                        <th class="th-sm">Id</th>
                        <th class="th-sm" style="width: 80px;">Status</th>
                        <th class="th-sm">Cosecha</th>
                        <th class="th-sm">Proveedor</th>
                        <th class="th-sm" style="width: 70px;">Contrato</th>
                        <th class="th-sm">Liquidación</th>
                        <th class="th-sm">Pacas</th>
                        <th class="th-sm">Tipo pago</th>
                        <th class="th-sm">Tipo Liq</th>
                        <th class="th-sm">Precio</th>
                        <th class="th-sm">Peso Prom</th>
                        <th class="th-sm">Metodo Pago</th>
                        <th class="th-sm" style="width: 70px;">Fecha</th>
                        <th class="th-sm">Importe</th>
                        <th class="th-sm">Prom</th>
                        <th class="th-sm">Factura</th>
                        <th class="th-sm">Comprobante Pago</th>
                        <th class="th-sm">Complemento de Pago</th>
                        <th class="no-exportar" style="width: 100px; position: sticky; right: 0px;"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="th-sm">Id</th>
                        <th class="th-sm" style="width: 70px;">Status</th>
                        <th class="th-sm">Cosecha</th>
                        <th class="th-sm">Proveedor</th>
                        <th class="th-sm" style="width: 70px;">Contrato</th>
                        <th class="th-sm">Liquidación</th>
                        <th class="th-sm">Pacas</th>
                        <th class="th-sm">Tipo pago</th>
                        <th class="th-sm">Tipo Liq</th>
                        <th class="th-sm">Precio</th>
                        <th class="th-sm">Peso Prom</th>
                        <th class="th-sm">Metodo Pago</th>
                        <th class="th-sm" style="width: 70px;">Fecha</th>
                        <th class="th-sm">Importe</th>
                        <th class="th-sm">Prom</th>
                        <th class="th-sm">Factura</th>
                        <th class="th-sm">Comprobante Pago</th>
                        <th class="th-sm">Complemento Pago</th>
                        <th class="no-exportar" style="width: 100px; position: sticky; right: 0px;"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <!--Modal para abrir la carga de Fac y Comp-->
    <div class="modal fade" id="modalFiles" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title1" id="exampleModalLabel"></h5>
                    <button id="closedI" type="button" class="btn-close" aria-label="Close"></button>
                </div>
                <form id="formFiles">
                    <div class="modal-body">
                        <div class="strike" id="">
                                <span><b>Archivos de Factura<b></span>
                        </div><br>
                        <div class="row justify-content-center align-items-center">
                            <div class="col-lg-2">
                                <button id="btn1" class="btn btn-primary">Cargar</button>
                            </div>
                            <div class="col-lg-5" id="documentoInv" style="display: none">
                                <div class="input-group mb-3">
                                    <input class="form-control me-2" type="text" name="actualfile_inv" id="actualfile_inv" placeholder="" readonly>
                                    <div id="descarga_inv">
                                        <button class="btn btn-success btn-sm viewfile_inv" title="Descargar Pdf">
                                            <i class="material-icons">file_download</i>
                                        </button>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-lg-5" id="documentoXml" style="display: none">
                                <div class="input-group mb-3">
                                    <input class="form-control me-2" type="text" name="actualXml" id="actualXml" placeholder="" readonly>
                                    <div id="descarga_Xml">
                                        <button class="btn btn-success btn-sm viewfile_Xml" title="Descargar Xml">
                                            <i class="material-icons">file_download</i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <div class="strike" id="txtComp" style="display: none">
                            <span><b>Complemento de Pago<b></span>
                        </div><br>
                        <div class="row justify-content-center align-items-center">
                            <div class="col-lg-2" id="btnCompl" style="display: none">
                                <button id="btn2" class="btn btn-primary">Cargar </button>
                            </div>
                            <div class="col-lg-5" id="complInv" style="display: none">
                                <div class="input-group mb-3">
                                    <input class="form-control me-2" type="text" name="actComp" id="actComp" placeholder="" readonly>
                                    <div id="descargaComp">
                                        <button class="btn btn-success btn-sm DownComp" title="Descargar Pdf">
                                            <i class="material-icons">file_download</i>
                                        </button>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-lg-5" id="complXml" style="display: none">
                                <div class="input-group mb-3">
                                    <input class="form-control me-2" type="text" name="actualXmlComp" id="actualXmlComp" placeholder="" readonly>
                                    <div id="descarga_XmlComp">
                                        <button class="btn btn-success btn-sm DownComp_xml" title="Descargar Xml">
                                            <i class="material-icons">file_download</i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <div class="modal-footer">
                        <button id="closeI" type="button" class="btn btn-primary" >Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--Modal para subir factura PDF y XML-->
    <div class="modal fade" id="modalInv" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title3" id="exampleModalLabel"></h5>
                    <button id="closeX" type="button" class="btn-close" aria-label="Close"></button>
                </div>
                <form id="formInv" >
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="filePdf" class="col-form-label">Seleccionar archivo PDF:</label>
                                    <input type="file" class="form-control" id="filePdf" name="filePdf" accept=".pdf" required onchange="extencion_Pdf()">
                                    <input type="hidden" id="ExtPdf" name="ExtPdf" />
                                 </div>
                                <div class="form-group"><br>
                                    <label for="fileXml" class="col-form-label">Seleccionar archivo XML:</label>
                                    <input type="file" class="form-control" id="fileXml" name="fileXml" accept=".xml,.XML" required onchange="extencion_Pdf()">
                                    <input type="hidden" id="ExtXml" name="ExtXml" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 1em;" id="avisoInv"></div>
                        <button id="close_Inv" type="button" class="btn btn-secondary" >Cancelar</button>
                        <button id="load_Inv" class="btn btn-success" disabled></button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!--Modal para subir Complemento PDF y XML-->
    <div class="modal fade" id="modalCompl" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title4" id="exampleModalLabel"></h5>
                    <button id="closeXC" type="button" class="btn-close" aria-label="Close"></button>
                </div>
                <form id="formCompl" >
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label for="filePdfC" class="col-form-label">Seleccionar archivo PDF:</label>
                                    <input type="file" class="form-control" id="filePdfC" name="filePdfC" accept=".pdf" required onchange="extencion_Complemento()">
                                    <input type="hidden" id="ExtPdfC" name="ExtPdfC" />
                                 </div>
                                <div class="form-group"><br>
                                    <label for="fileXmlC" class="col-form-label">Seleccionar archivo XML:</label>
                                    <input type="file" class="form-control" id="fileXmlC" name="fileXmlC" accept=".xml,.XML" required onchange="extencion_Complemento()">
                                    <input type="hidden" id="ExtXmlC" name="ExtXmlC" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 1em;" id="avisoCompl"></div>
                        <button id="close_comp" type="button" class="btn btn-secondary" >Cancelar</button>
                        <button id="load_comp" class="btn btn-success" disabled>Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>

<?php 
    endif;
?>
<style>
    .custom-btn {
        padding: 4px 6px; /* Ajusta el espacio interno del botón */
        height: 31px;     /* Ajusta la altura */
    }
    .d-none {
        visibility: hidden;
        width: 0;
        height: 0;
        padding: 0;
        border: none;
    }
</style>