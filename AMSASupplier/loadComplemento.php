<?php

include_once 'bd/conexion.php';
require __DIR__.'/vendor/autoload.php';
date_default_timezone_set("America/Mexico_City");

use Aws\S3\S3Client; 
use Aws\Exception\AwsException; 

$objeto = new Conexion();
$conexion = $objeto->Conectar();
$LiqID = $_POST['LiqID'];
$id = $_POST['id'];
$fechaload = date('Y-m-d h:i:s a', time());  
$bucket = 'pruebasportal'; // bucket de pruebas
//$bucket = 'portal-liq';


$ticketPdf = $_FILES['filePdfC']['name']; // PDf inv
$extPdf = $_POST['ExtPdfC'];

$ticketXml = $_FILES['fileXmlC']['name']; // Archivo XML
$extXml = $_POST['ExtXmlC'];

// Consultar si ya hay archivos cargados 
$consulta = "SELECT pdfComp, xmlComp FROM amsadb1.Liquidation WHERE IdLiq = '$id'"; 
$resultado = $conexion->prepare($consulta);
$resultado->execute();
$datos = $resultado->fetch();
$extInvPdf = $datos['pdfComp'];
$extInvXml = $datos['xmlComp'];

$s3 = new S3Client([
    'version'     => 'latest',
    'region'      => 'us-east-2', //bucket de pruebas
    //'region'      => 'us-east-1',
    'credentials' => [
        'key'    => 'AKIAT442VUCJQXPEFVZW',
        'secret' => '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',
    ],
]);


// 1. Borrar el archivo PDF si ya existe
if($extInvPdf != null){
    try {
        $result = $s3->deleteObject([
            'Bucket' => $bucket,
            'Key'    => "Complemento pago-".$LiqID.".".$extInvPdf
        ]);

        if ($result['DeleteMarker']) {
            $data = "actualizado";
        } else {
            $data = "Error al actualizar el PDF";
        }
    } catch (S3Exception $e) {
        exit('Error: ' . $e->getAwsErrorMessage() . PHP_EOL);
    }
}

// 2. Borrar el archivo XML si ya existe
if($extInvXml != null){
    try {
        $result = $s3->deleteObject([
            'Bucket' => $bucket,
            'Key'    => "Complemento pago-".$LiqID.".".$extInvXml
        ]);

        if ($result['DeleteMarker']) {
            $data = "actualizado";
        } else {
            $data = "Error al actualizar el XML";
        }
    } catch (S3Exception $e) {
        exit('Error: ' . $e->getAwsErrorMessage() . PHP_EOL);
    }
}

// 3. Subir el nuevo archivo PDF
try {
    if ($_FILES['filePdfC']['tmp_name']) {
        $result = $s3->putObject([
            'Bucket' => $bucket,
            'Key'    => "Complemento pago-".$LiqID.".".$extPdf,
            'SourceFile' => $_FILES['filePdfC']['tmp_name']
        ]);

        $consulta = "UPDATE amsadb1.Liquidation SET pdfComp='$extPdf' WHERE IdLiq = '$id'"; 		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();    
    }

    // 4. Subir el nuevo archivo XML
    if ($_FILES['fileXmlC']['tmp_name']) {
        $result = $s3->putObject([
            'Bucket' => $bucket,
            'Key'    => "Complemento pago-".$LiqID.".".$extXml,
            'SourceFile' => $_FILES['fileXmlC']['tmp_name']
        ]);

        $consulta = "UPDATE amsadb1.Liquidation SET xmlComp='$extXml' WHERE IdLiq = '$id'"; 		
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();    
    }


    // 5. Actualizar la fecha de carga
    /* $Hoy = date('Y-m-d');
    $consulta = "UPDATE amsadb1.Liquidation SET datInv='$Hoy' WHERE IdLiq = '$id'"; 
    $resultado = $conexion->prepare($consulta);
    $resultado->execute(); */

} catch (S3Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

// 6. Enviar respuesta al frontend
$data = [
    'pdf' => "Complemento pago-".$LiqID.".".$extPdf,
    'xml' => "Complemento pago-".$LiqID.".".$extXml
];

print json_encode($data, JSON_UNESCAPED_UNICODE); // Enviar respuesta en formato JSON a AJAX
?>