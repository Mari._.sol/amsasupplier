$(document).ready(function() {

    $('#tablaLiquidation tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );
    
    opcion = 1;
    var GinID  = $.trim($('#GinID').val());
    var usuario  = $.trim($('#usuario').val());
    
    tablaLiquidation = $('#tablaLiquidation').DataTable({
        //para usar los botones   
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
            {
                extend:    'excelHtml5',
                text:      '<i class="fas fa-file-excel"></i> ',
                titleAttr: 'Export to Excel',
                className: 'btn btn-success',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fas fa-file-pdf"></i> ',
                titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                    columns: ":not(.no-exportar)"
                }
            }
        ],
        
        initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
    
                $('input', $(this.footer())).on('keyup change clear', function () {
                    if (that.search() !== this.value) {
                        that
                        .search(this.value, true, false)
                        .draw();
                    }
                });
            });
        },
        "order": [ 5, 'desc' ],
        "scrollX": true,
        "scrollY": "50vh",
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500], 
        fixedColumns:   {
            left: 0,
            right: 1
        },
        "ajax":{            
            "url": "bd/CrudLiq.php", 
            "method": 'POST', 
            "data":{opcion:opcion, GinID:GinID, usuario:usuario}, //enviamos opcion 1 para que haga un SELECT
            "dataSrc":""
        },
        "columnDefs": [
            {
                "targets": 0, // Índice de la columna "IdLiq"
                "className": "d-none" // Oculta visualmente la columna
            }
        ],
        "columns":[
            {"data": "IdLiq"},
            {"data": "prov",
                "render": function(data, type, row) { 
                    if (row.filePago != null && row.filePago != '') {
                        return 'Pagado';
                    } else if (row.fileFactura != null && row.fileFactura != '') {
                        return 'Pago pendiente';
                    } else if (row.prov != '0') {
                        return 'Esp Fact/Nota';
                    } else {
                        return 'En proceso';
                    }
                }
            },
            {"data": "Crop"},
            {"data": "Proveedor"},
            {"data": "Contrato"},
            {"data": "LiqID"},
            {"data": "Pacas"},
            {"data": "TipoPago"},
            {"data": "TipoLiq"},
            {"data": "Precio",
                "render": function (data, type, row) {
                    if (data === '' || data === null || data === "0") {
                        return ''; 
                    } else {
                        return '$' + parseFloat(data).toFixed(2);
                    }
                }
            },
            {"data": "PesoProm"},
            {"data": "payMetod",
                "render": function(data) { 
                    if (data == '1') {
                        return 'PPD-99';
                    } else if (data == '2') {
                        return 'Pue-Trans';
                    } else if (data == '3') {
                        return 'Pue-Comp';
                    } else {
                        return '';
                    }
                }
            },
            {"data": "datProv"},
            {"data": "ConDescuento",
                "render": function (data, type, row) {
                    if (data === '' || data === null || data === "0") {
                        return ''; 
                    } else {
                        return '$' + parseFloat(data).toLocaleString('en-US');
                    }
                }
            },            
            {"data": "Prom",
                    "render": function (data, type, row) {
                    if (data === '' || data === null || data === "0") {
                        return ''; 
                    } else {
                        return '$' + parseFloat(data).toFixed(2); 
                    }
                }
            },
            {"data": "fileFactura",
                "render": function(data) { 
                    if (data === '' ) {
                        return ''; 
                    } else {
                        return data == null ? 'NO' : 'SI'
                    }
                }
            },
            {"data": "filePago",
                "render": function(data) { 
                    if (data === '' ) {
                        return ''; 
                    } else {
                        return data == null ? 'NO' : 'SI'
                    }
                }
            },
            {"data": "pdfComp",
                "render": function(data) { 
                    if (data === '' ) {
                        return ''; 
                    } else if (data === "N/A") {
                        return data; 
                    } else {
                        return data == null ? 'NO' : 'SI'
                    }
                }
            },
            {"data": "acciones",
                "render": function(data, type, row){ 
                    if (data == '1') {
                        if (row.filePago != null) {  
                            return "<div class='d-flex justify-content-start'><button class='btn btn-primary custom-btn btnInv' title='Cargar Factura'><i class='material-icons fs-5'>upload_file</i></button><button class='btn btn-success custom-btn viewExcel' title='Descargar Liquidación Xlsx'><i class='material-icons fs-5'>save_alt</i></button><button class='btn btn-warning custom-btn DowdPrice' title='Descargar Precio Img'><i class='material-icons fs-5'>image</i></button><button class='btn btn-danger custom-btn DowdPay' title='Comprobante de Pago'><i class='material-icons fs-5'>picture_as_pdf</i></button></div>"
                        }else{
                            return "<div class='d-flex justify-content-start'><button class='btn btn-primary custom-btn btnInv' title='Cargar Factura'><i class='material-icons fs-5'>upload_file</i></button><button class='btn btn-success custom-btn viewExcel' title='Descargar Liquidación Xlsx'><i class='material-icons fs-5'>save_alt</i></button><button class='btn btn-warning custom-btn DowdPrice' title='Descargar Precio Img'><i class='material-icons fs-5'>image</i></button></div>"
                        }
                    } else if (data == '') {
                        return ""
                    } 
                }
            },
        ],
    });     
    $('.dataTables_length').addClass('bs-select');  

    $('#manual').on('click', function(e) {
        e.preventDefault(); 
        window.open("./bd/Manual-Liq-V1.pdf");
    });
    
});

// modal para subir la Factura
$(document).on("click", ".btnInv", function(e){
    e.preventDefault(); 
    fila = $(this).closest("tr");
    id = $(this).closest('tr').find('td:eq(0)').text();
    prov = $(this).closest('tr').find('td:eq(3)').text();
    contrato = $(this).closest('tr').find('td:eq(4)').text();
    liq = $(this).closest('tr').find('td:eq(5)').text();
    pacas = $(this).closest('tr').find('td:eq(6)').text();
    tipoLiq = $(this).closest('tr').find('td:eq(8)').text();
    payM = $(this).closest('tr').find('td:eq(11)').text();
    subject = 'Archivos de Factura, Liquidación '+ liq +' - ' + pacas + ' bc'; 

    $("#formFiles").trigger("reset");
    $(".modal-header").css( "background-color", "#17562c");
    $(".modal-header").css( "color", "white" );
    $('#modalFiles').modal('show');
    $(".modal-title1").text("Liquidacion - " + liq);

    if (payM === "PPD-99") {
        document.getElementById('txtComp').style.display = 'block';
        document.getElementById('btnCompl').style.display = 'block';
    }else{
        document.getElementById('txtComp').style.display = 'none';
        document.getElementById('btnCompl').style.display = 'none'; 
    }

    $.ajax({
        url: "bd/CrudLiq.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:2,liq:liq, tipoLiq:tipoLiq},    
        success: function(data){
            opts = JSON.parse(data);
            if (opts[0].fileFactura === null || opts[0].fileXML === null) {
                document.getElementById('documentoInv').style.display = 'none';
                document.getElementById('documentoXml').style.display = 'none';
                $(".modal-title").text("Cargar Factura " + liq);
                $("#load_Inv").text("Cargar Factura"); 
            }
            else{
                document.getElementById('documentoInv').style.display = 'block';
                document.getElementById('documentoXml').style.display = 'block';
                $("#actualfile_inv").val("Fac-"+ liq +"."+ opts[0].fileFactura)
                $("#actualXml").val("Fac-"+ liq +"."+ opts[0].fileXML)
                $(".modal-title").text("Actualizar Factura " + liq);
                $("#btn1").text("Actualizar"); 
                $("#load_Inv").text("Actualizar Factura");     
            }

            if (opts[0].pdfComp === null || opts[0].xmlComp === null) {
                document.getElementById('complInv').style.display = 'none';
                document.getElementById('complXml').style.display = 'none';
                $(".modal-title").text("Cargar complemento" + liq);
                $("#load_comp").text("Cargar complemento"); 
            }
            else{
                document.getElementById('complInv').style.display = 'block';
                document.getElementById('complXml').style.display = 'block';
                $("#actComp").val("Complemento pago-"+ liq +"."+ opts[0].pdfComp)
                $("#actualXmlComp").val("Complemento pago-"+ liq +"."+ opts[0].xmlComp)
                $(".modal-title").text("Actualizar complemento " + liq);
                $("#btn2").text("Actualizar"); 
                $("#load_comp").text("Actualizar complemento");     
            }

            /* if (opts[0].filePago != null) {
                console.log("Si hay com Pago")
            }else{
                console.log("no hay com Pago")  
            } */
        }
    });

    $('#btn1').on('click', function(e) {
        e.preventDefault(); 
        $("#formInv").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title3").text("Cargar Factura");
        $('#modalInv').modal('show');
        
        
        
        $('#load_Inv').off('click').on('click', function(e) {
            e.preventDefault();  
            $('#avisoInv').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
            
            var filePdf = $('#filePdf').prop('files')[0]; 
            var ExtPdf = $('#ExtPdf').val();
            var fileXml = $('#fileXml').prop('files')[0]; 
            var ExtXml = $('#ExtXml').val();
            var form_data = new FormData();  

            form_data.append('filePdf', filePdf);
            form_data.append('ExtPdf', ExtPdf);
            form_data.append('fileXml', fileXml);
            form_data.append('ExtXml', ExtXml);
            form_data.append('LiqID', liq);
            form_data.append('id', id);
        
            $.ajax({
                url: 'LoadInv.php', 
                datatype: "json",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(data) {              
                    var opts = JSON.parse(data);
                    console.log(opts)
                    if (opts.pdf != null && opts.xml!=null) {
                        usuario = $('#dropdownMenuLink2').text();
                        let formData = new FormData();  
                        formData.append("prov", prov);
                        formData.append("pacas", pacas);
                        formData.append("subject", subject);
                        formData.append("usuario", usuario);
                        formData.append("contrato", contrato);
                        formData.append("LiqID", liq);

                        $.ajax({
                            url: "bd/mailFactura.php",
                            type: "POST",
                            datatype:"json",  
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function(response){
                                alert("Factura guardada con éxito...");
                                document.getElementById('documentoInv').style.display = 'block';
                                document.getElementById('documentoXml').style.display = 'block';
                                $("#actualfile_inv").val(opts.pdf);
                                $("#actualXml").val(opts.xml);
                                $('#avisoInv').html(""); 
                                $('#modalInv').modal('hide');
                                $("#btn1").text("Actualizar ");
                                $('#load_Inv').prop('disabled', true);
                                tablaLiquidation.ajax.reload(null, false);
                            },
                        })
                    }
                },
                error: function(jqXHR,) {
                    $('#avisoInv').html(""); 
                    alert("Error al cargar los archivo");
                }
            });
        });


        // cerrar modal de cargar factura
        $('#close_Inv, #closeX').on('click', function(e) {
            e.preventDefault();
            var $el = $('#filePdf');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            
            var $elXml = $('#fileXml');
            $elXml.wrap('<form>').closest('form').get(0).reset();
            $elXml.unwrap();
            
            $('#modalInv').modal('hide');
            $('#load_Inv').prop('disabled', true);
        });
    });
    
    //abrir modal para complemento 
    $('#btn2').on('click', function(e) {
        e.preventDefault(); 
        $("#formCompl").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title4").text("Cargar complemento");
        $('#modalCompl').modal('show');

        $('#load_comp').off('click').on('click', function(e) {
            e.preventDefault();  
            $('#avisoCompl').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
            
            var filePdfC = $('#filePdfC').prop('files')[0]; 
            var ExtPdfC = $('#ExtPdfC').val();
            var fileXmlC = $('#fileXmlC').prop('files')[0]; 
            var ExtXmlC = $('#ExtXmlC').val();
            var form_data = new FormData();  

            form_data.append('filePdfC', filePdfC);
            form_data.append('ExtPdfC', ExtPdfC);
            form_data.append('fileXmlC', fileXmlC);
            form_data.append('ExtXmlC', ExtXmlC);
            form_data.append('LiqID', liq);
            form_data.append('id', id);
        
            $.ajax({
                url: 'loadComplemento.php', 
                datatype: "json",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(data) {              
                    var opts = JSON.parse(data);
                    console.log(opts)
                    if (opts.pdf != null && opts.xml!=null) {
                        usuario = $('#dropdownMenuLink2').text();
                        subject = 'Complemento de pago, Liquidación '+ liq +' - ' + pacas + ' bc';
                        let formData = new FormData();  
                        formData.append("prov", prov);
                        formData.append("pacas", pacas);
                        formData.append("subject", subject);
                        formData.append("usuario", usuario);
                        formData.append("contrato", contrato);
                        formData.append("LiqID", liq);

                        $.ajax({
                            url: "bd/mailComp.php",
                            type: "POST",
                            datatype:"json",  
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function(response){
                                alert("Complemento de pago guardado con éxito...");
                                document.getElementById('complInv').style.display = 'block';
                                document.getElementById('complXml').style.display = 'block';
                                $("#actComp").val(opts.pdf);
                                $("#actualXmlComp").val(opts.xml);
                                $('#avisoCompl').html(""); 
                                $('#modalCompl').modal('hide');
                                $('#load_comp').prop('disabled', true);
                                $("#btn2").text("Actualizar");
                                tablaLiquidation.ajax.reload(null, false);
                            },
                        })
                    }
                },
                error: function(jqXHR,) {
                    $('#avisoInv').html(""); 
                    alert("Error al cargar los archivo");
                }
            });
        });


        // cerrar modal de cargar factura
        $('#close_comp, #closeXC').on('click', function(e) {
            e.preventDefault();
            var $el = $('#filePdfC');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            
            var $elXml = $('#fileXmlC');
            $elXml.wrap('<form>').closest('form').get(0).reset();
            $elXml.unwrap();
            
            $('#modalCompl').modal('hide');
            $('#load_comp').prop('disabled', true);
        });
    });

    // cerrar modal de cargar factura
    $('#closeI, #closedI').on('click', function(e) {
        e.preventDefault();
        $('#modalFiles').modal('hide');
        tablaLiquidation.ajax.reload(null, false);
    });
});

// Descargar Excel de liq
$(document).on("click", ".viewExcel", function(e){
    e.preventDefault(); 
    fila = $(this).closest("tr");
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadLiq.php?liqId="+liqId);
});

$(document).on("click", ".DowdPrice", function(e){
    e.preventDefault(); 
    fila = $(this).closest("tr");
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadPrice.php?liqId="+liqId);
});

$(document).on("click", ".DowdPay", function(e){
    e.preventDefault(); 
    fila = $(this).closest("tr");
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadPago.php?liqId="+liqId);
});

// descargar factura 
$(document).on("click", ".viewfile_inv", function(e){
    e.preventDefault(); 
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadInv.php?liqId="+liqId);
});

$(document).on("click", ".viewfile_Xml", function(e){
    e.preventDefault(); 
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadXml.php?liqId="+liqId);
});

// decargar complemento de factura 
$(document).on("click", ".DownComp", function(e){
    e.preventDefault(); 
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadComp.php?liqId="+liqId);
});

$(document).on("click", ".DownComp_xml", function(e){
    e.preventDefault(); 
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadCompXml.php?liqId="+liqId);
});

$(document).on("click", ".DownComp_xml", function(e){
    e.preventDefault(); 
    liqId = parseInt(fila.find('td:eq(0)').text()); //capturo el ID
    window.open("downloadCompXml.php?liqId="+liqId);
});

function extencion_Pdf() {
    var filenamePdf = $("#filePdf").val();
    var extensionPdf = filenamePdf.split('.').pop().toLowerCase();  
    if (filenamePdf === "") {
        $('#load_Inv').prop('disabled', true);
    } else if (extensionPdf === 'pdf') { 
        $("#ExtPdf").val(extensionPdf);
    } else {
        alert("Extensión no válida para el archivo PDF");
        $('#load_Inv').prop('disabled', true);
    }

    var filenameXml = $("#fileXml").val();
    var extensionXml = filenameXml.split('.').pop().toLowerCase();
    if (filenameXml === "") {
        $('#load_Inv').prop('disabled', true);
    } else if (extensionXml === 'xml') {
        $("#ExtXml").val(extensionXml); 
    } else {
        alert("Extensión no válida para el archivo XML");
        $('#load_Inv').prop('disabled', true);
    }

    if (filenameXml === "" || filenamePdf === "" ){
        $('#load_Inv').prop('disabled', true);
    }else{
        $('#load_Inv').prop('disabled', false);
    }
}



function extencion_Complemento() {
    var filenamePdfC = $("#filePdfC").val();
    var extensionPdfC = filenamePdfC.split('.').pop().toLowerCase();  
    if (filenamePdfC === "") {
        $('#load_comp').prop('disabled', true);
    } else if (extensionPdfC === 'pdf') { 
        $("#ExtPdfC").val(extensionPdfC);
    } else {
        alert("Extensión no válida para el archivo PDF");
        $('#load_comp').prop('disabled', true);
    }

    var filenameXmlC = $("#fileXmlC").val();
    var extensionXmlC = filenameXmlC.split('.').pop().toLowerCase();
    if (filenameXmlC === "") {
        $('#load_comp').prop('disabled', true);
    } else if (extensionXmlC === 'xml') {
        $("#ExtXmlC").val(extensionXmlC); 
    } else {
        alert("Extensión no válida para el archivo XML");
        $('#load_comp').prop('disabled', true);
    }

    if (filenameXmlC === "" || filenamePdfC === "" ){
        $('#load_comp').prop('disabled', true);
    }else{
        $('#load_comp').prop('disabled', false);
    }
}
